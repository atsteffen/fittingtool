The FittingTool can be run using eclipse as follows:

1) Download and unzip the source from:  https://bitbucket.org/atsteffen/fittingtool
2) Install and open Eclipse
3) Create a new java project in Eclipse
4) Import the source into the project
	- Right click the project: Import > General > File System
	- Browse to the unzipped fitting tool folder
	- Hit finish
5) Add the jogl and gluegen library jars
	- Right click the project: Properties > Java build path > Libraries
	- Hit Add jars…
	- Navigate to the /lib directory and select gluegen-rt.jar and jogl.jar, and hit Ok
6) Add jogl and gluegen's compiled dependencies
	- Expand the eclipse project and expand Referenced Libraries
	- Right click on gluegen-rt.jar: Properties > Native Library
	- Browse to the lib directory of the appropriate OS, and hit Ok
	- Right click jogl.jar and do the same
