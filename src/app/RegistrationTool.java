package app;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.media.opengl.GLCapabilities;
import javax.swing.JPanel;
import javax.swing.JSplitPane;

import app.tools.cross_sections.CuttingTool;
import app.tools.structure.Landmark;
import app.tools.structure.Mesh;
import app.tools.topology.Vertex;
import app.tools.utils.Log;
import app.tools.utils.MeshLoaderUtils;
import app.tools.utils.RegistrationUtils;
import app.ui.controls.PermanentOptions;
import app.ui.controls.TabbedOptions;
import app.ui.controls.InteractiveRegistrationPanel.RegistrationMode;
import app.ui.gl.Viewer2D;
import app.ui.gl.Viewer3D;

/**
 * This class serves as the controller, and contains the model (mesh and landmarks), viewers (2D and 3D), and option panels.
 * 
 * @author Adam Steffen
 */
public class RegistrationTool extends JPanel {
	private static final long serialVersionUID = 1L;
	
	public static final float SCALE = 1.0f;	//Automatically inherited by CuttingTool, Viewer2D, and Viewer3D.

	//-------------------------------------------------------------------
	// Properties
	//-------------------------------------------------------------------

	// model
	private Mesh basemodel;
	private Mesh submodel; 
	private int subdivLevel;
	private List<Landmark> landmarks;
	
	// options
	private RegistrationMode registrationMode = RegistrationMode.GLOBAL;
	private List<Integer> activeMaterials;
	
	// temporary map, used for local registration
	private Map<Integer,Integer> partialVertMap;
	
	// viewers/panels
	private CuttingTool cutter;
	private Viewer3D viewer;	
	private Viewer2D selector;
	private TabbedOptions upperControls;
	private PermanentOptions lowerControls;
	private JSplitPane splitPane;
	
	//-------------------------------------------------------------------
	// Constructors
	//-------------------------------------------------------------------

	/**
	 * Initialize the central main container for mesh data, landmarks, viewers and option panels.
	 * @param parent containing frame used to set the relative dimensions of contents
	 */
	public RegistrationTool(Container parent) {
		activeMaterials = new ArrayList<Integer>();
		activeMaterials.add(-2);
		activeMaterials.add(-1);
		landmarks = new ArrayList<Landmark>();
		cutter = new CuttingTool(this);
		init(parent);
		Log.log("GUI and model have launched. Program initialized.");
	}
	
	//-------------------------------------------------------------------
	// Initialization methods
	//-------------------------------------------------------------------

	/**
	 * Initialize option panels and viewers
	 * @param parent application frame used to set the relative dimensions of contents
	 */
	private void init(Container parent) {
		setLayout(new BorderLayout());	
		System.setProperty("sun.awt.noerasebackground", "true");	// This might be helping with flickering and performance?

		GLCapabilities glCap = new GLCapabilities();			//Create a GLCapabilities object for the requirements for GL
		glCap.setDoubleBuffered(true);								//Enable double buffer 
		glCap.setHardwareAccelerated(true);						//Enable hardware acceleration				

		viewer = new Viewer3D(this, glCap);																				
		selector = new Viewer2D(this, glCap);
		upperControls = new TabbedOptions(this, viewer);
		lowerControls = new PermanentOptions(this);											

		initSplitPane(parent);
		add(upperControls, BorderLayout.NORTH);
		add(splitPane, BorderLayout.CENTER);
		add(lowerControls,BorderLayout.SOUTH);
	}

	/** 
	 * Initializes the split-pane containing the two GLPanel instances.  
	 */
	private void initSplitPane(Container parent) {
		splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, viewer.getGLCanvas(), selector.getGLCanvas());
		splitPane.setOneTouchExpandable(true);
		splitPane.setBackground(Color.LIGHT_GRAY);
		splitPane.setSize(new Dimension(parent.getWidth(),100));
		splitPane.setDividerLocation(0.5f);
	}

	/**
	 * Start the animation of 2D and 3D viewers
	 */
	public void start() {
		viewer.start();
		selector.start(); 
	}
	
	//-------------------------------------------------------------------
	// Methods: Getters and setters
	//-------------------------------------------------------------------
	
	/**
	 * @return Current mode of registration (local or global)
	 */
	public RegistrationMode getRegistrationMode() { return registrationMode; }
	/**
	 * @param mode Mode of registration to assume (local or global)
	 */
	public void setRegistrationMode(RegistrationMode mode) { registrationMode = mode; }
	
	/**
	 * @return Entire set of currently visible landmarks. Does not include landmarks which have been saved and are
	 * contributing to the fitting term of the global registration matrix
	 */
	public List<Landmark> getLandmarks() { return landmarks; }
	/**
	 * @param landmarks Currently visible landmarks
	 */
	public void setLandmarks(List<Landmark> landmarks) { this.landmarks = landmarks; }
	/**
	 * Remove all visible landmarks
	 */
	public void clearLandmarks() { this.landmarks = new ArrayList<Landmark>(); }
	/**
	 * Add landmark to the set of visible landmarks
	 * @param lm New landmark to add
	 */
	public void addLandmark(Landmark lm) { landmarks.add(lm); }
	
	/**
	 * @return Viewer for the 3D rendering of the mesh and landmarks
	 */
	public Viewer3D getViewer3D() { return viewer; }
	/**
	 * @return Viewer for the 2D rendering of a slice of the mesh and nearby landmarks
	 */
	public Viewer2D getViewer2D() { return selector; }
	/**
	 * @return Container for a set of tools for translating the 3D mesh into 2D slices
	 */
	public CuttingTool getCutter() { return cutter; }
	
	/**
	 * @return Two active landmark materials, these will be the materials of new landmarks added at the time
	 */
	public List<Integer> getActiveMaterials() { return activeMaterials; }
	/**
	 * Set one of the active materials
	 * @param i Which of the two materials to set
	 * @param material index for the material color being set
	 */
	public void setActiveMaterials(int i, int material) {
		if (activeMaterials != null && activeMaterials.size() > i) {
			activeMaterials.set(i, material);
		}	
	}
	
	/**
	 * @return Subdivided full mesh
	 */
	public Mesh getMesh() { return submodel; }
	/**
	 * @return Current subdivision level of the subdivided mesh
	 */
	public int getSubLevel() { return subdivLevel; }
	/**
	 * @return Unsubdivided full mesh
	 */
	public Mesh getBaseMesh() { return basemodel; }	
	/**
	 * Set the current base mesh, reload all panels and viewers.
	 * @param mesh New working base mesh
	 */
	public void setBaseMesh(Mesh mesh) { this.basemodel = mesh; reloadMesh(); }
	/**
	 * Move only those vertices of the current base mesh that correspond to vertices in a partial mesh as determined
	 * by the partialVertMap
	 * @param partial A portion of the base mesh corresponding to two regions of the base mesh
	 */
	public void setPartialMesh(Mesh partial) {
		this.basemodel.setPartialMesh(partialVertMap, partial, getActiveMaterials());
		reloadMesh();
	}	
	/**
	 * @return Get the portion of the base mesh corresponding to the two regions on the boundary determined by the activeMaterials.
	 * returns <b><code>null</code></b> if the activeMaterials are identical, or either contains an unknown material or boundary material.
	 */
	public Mesh getPartialMesh() {
		partialVertMap = new LinkedHashMap<Integer,Integer>();
		return getBaseMesh().getPartialMesh(partialVertMap, activeMaterials);
	}	
	/**
	 * Subdivide the base mesh, and refresh all viewers and controllers
	 */
	public void reloadMesh() {
		submodel = (Mesh) basemodel.clone();
		submodel.subdivide();
		subdivLevel = submodel.getSubDivLevel();
		cutter.init();
		viewer.refresh();
		cutter.refreshHard();
	}
	
	//-------------------------------------------------------------------
	// Methods: IO methods
	//-------------------------------------------------------------------

	/**
	 * Load landmarks from a properly formatted landmark file
	 * @param filepath Path to landmark file
	 */
	public void loadLandmarks(String filepath) {
		try {
			this.landmarks.addAll(MeshLoaderUtils.loadLandmarks(new File(filepath)));
		} catch (Exception e) {
			Log.warn("Failed to load landmarks from specified file, this file may be incorrectly formatted. " + 
					"Make sure the first line in the file is the word: landmarks. In following lines list " +
					"landmark vertices: x y z and materials, separated by spaces");
		}
	}

	/**
	 * Load a mesh from a properly formatted mesh file
	 * @param filepath Path to mesh file
	 */
	public void loadMesh(File filepath) {
		try {
			this.basemodel = MeshLoaderUtils.loadMesh(filepath);
		} catch (IOException e) {
			Log.warn("Failed to load mesh from specified file, this file may be incorrectly formatted. " + 
					"Make sure the first line in the file is the word: poly, and that the second line has number of vertices " +
					"and number of polygons separated by a space (Examples: 10 10). Then list vertices. Finally, list polygons " +
					"as type (4 or 8), vertex indices, and materials separated by spaces.");
		}
		submodel = (Mesh) basemodel.clone();
		submodel.subdivide();
		subdivLevel = submodel.getSubDivLevel();
		cutter.init();
		viewer.loadMesh(submodel);
		cutter.refreshHard();
		upperControls.init();
		upperControls.validate();
		RegistrationUtils.startRegistrationSession(getBaseMesh(), getSubLevel());
	}
	
	/**
	 * Write the base mesh to a file
	 * @param filepath File path to write the mesh
	 */
	public void writeMesh(String filepath) {
		try {
			MeshLoaderUtils.writeMesh(basemodel, new File(filepath));
		} catch (IOException e) {
			Log.warn("Failed to write mesh to file: " + filepath + ". Check permissions.");
		}		
	}
	
	/**
	 * Write a set of meta data needed by the mesh loader downstream
	 * @param filepath Path to the meta data file
	 * @throws IOException
	 */
	public void writeMeta(String filepath) throws IOException {
		if (getViewer2D().getStreamer() != null) {
			Vertex origin = getViewer2D().getStreamer().getOrigin();
			float spacing = getViewer2D().getStreamer().getSpacingz();
			BufferedWriter bw = new BufferedWriter(new FileWriter(new File(filepath)));
			try {
				bw = new BufferedWriter(new FileWriter(new File(filepath)));
				bw.write("#origin: " + origin.getX() + "," + origin.getY() + "," + origin.getZ());
				bw.write("#spacing: " + spacing);
			} finally {
				bw.close();
			}
		}
	}
}

