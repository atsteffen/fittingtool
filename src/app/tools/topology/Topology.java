package app.tools.topology;

import java.util.ArrayList;
import java.util.List;


/** Topology.java
 * <br>
 * Stores ArrayLists containing the different features of a Mesh. 
 * Includes crease points, crease edges, crease faces, and all polyhedra.
 * 
 * @author Don McCurdy
 *
 */
public class Topology implements Cloneable {
	private List<Integer> 			points;
	private List<Edge> 				edges;
	private List<Face>				faces;
	private List<Polyhedron>	polyhedra;
	
	/** Initializes mesh topology, given arraylists for each type of element.
	 * @param vertices
	 * @param edges
	 * @param faces
	 * @param polyhedra
	 */
	public Topology (List<Integer> v, List<Edge> e, List<Face> f, List<Polyhedron> p) {
		points = v;
		edges = e;
		faces = f;
		polyhedra = p;
	}

	/** Returns a list of all polyhedra in the mesh.
	 * 
	 * @return polyhedra
	 */
	public List<Polyhedron> getPolyhedra() {
		return polyhedra;
	}
	
	/** Returns a list of all crease points in the mesh.
	 * 
	 * @return crease_points
	 */
	public List<Integer> getPoints() {
		return points;
	}
	
	/** Returns a list of all crease edges in the mesh.
	 * 
	 * @return crease_edges
	 */
	public List<Edge> getEdges() {
		return edges;
	}
	
	/** Returns a list of all crease faces in the mesh.
	 * 
	 * @return crease_faces
	 */
	public List<Face> getFaces() {
		return faces;
	}
	
	@Override
	public Object clone() {
		try {
			Topology result = (Topology) super.clone();
			result.edges = new ArrayList<Edge>();
			for (Edge e : edges) {
				result.edges.add((Edge)e.clone());
			}
			result.faces = new ArrayList<Face>();
			for (Face f : faces) {
				result.faces.add((Face)f.clone());
			}
			result.polyhedra = new ArrayList<Polyhedron>();
			for (Polyhedron p : polyhedra) {
				result.polyhedra.add((Polyhedron)p.clone());
			}
			return result;
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
			return null;
		}	
	}
}
