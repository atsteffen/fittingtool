package app.tools.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class ImageUtils {
	
	public static void largestConnectForeground(int[] matrix, int width, int height) {
		List<Integer> currentLargest = new ArrayList<Integer>();
		for (int i = 0; i < width; i++){
			for (int j = 0; j < height; j++){
				if (matrix[height*i+j] == 1) {
					List<Integer> temp = flood8Connect(matrix, i*height+j, width, height);
					if (temp.size() > currentLargest.size()) {
						currentLargest = temp;
					}
					if (currentLargest.size() > (width*height/2)){
						break;
					}
				}
			}
		}
		for (int i = 0; i < width; i++){
			for (int j = 0; j < height; j++){
				matrix[height*i+j] = 0;
			}
		}
		for(Integer i : currentLargest){
			matrix[i] = 1;
		}
	}
	
	public static void largestConnectBackground(int[] matrix, int width, int height) {
		List<Integer> currentLargest = new ArrayList<Integer>();
		for (int i = 0; i < width; i++){
			for (int j = 0; j < height; j++){
				if (matrix[height*i+j] == 0) {
					List<Integer> temp = flood8Connect(matrix, i*height+j, width, height);
					if (temp.size() > currentLargest.size()) {
						currentLargest = temp;
					}
					if (currentLargest.size() > (width*height/2)){
						break;
					}
				}
			}
		}
		for (int i = 0; i < width; i++){
			for (int j = 0; j < height; j++){
				matrix[height*i+j] = 0;
			}
		}
		for(Integer i : currentLargest){
			matrix[i] = 1;
		}
	}

	public static List<Integer> flood8Connect(int[] matrix, int index, int width, int height) {
		List<Integer> result = new ArrayList<Integer>();
		Stack<Integer> stack = new Stack<Integer>();
		stack.push(index);
		result.add(index);
		matrix[index] = -1;
		while (!stack.isEmpty()) {
			int i = stack.pop();
			int w = i/height;
			int h = i%height;
			if (w != 0) {
				if (matrix[i-height] == 0) {
					result.add(i-height);
					stack.push(i-height);
					matrix[i-height] = -1;
				}
				if (h != 0) {
					if (matrix[i-height-1] == 0) {
						result.add(i-height-1);
						stack.push(i-height-1);
						matrix[i-height-1] = -1;
					}
				}
				if (h != height-1){
					if (matrix[i-height+1] == 0) {
						result.add(i-height+1);
						stack.push(i-height+1);
						matrix[i-height+1] = -1;
					}
				}
			}
			if (w != width-1) {
				if (matrix[i+height] == 0) {
					result.add(i+height);
					stack.push(i+height);
					matrix[i+height] = -1;
				}
				if (h != 0) {
					if (matrix[i+height-1] == 0) {
						result.add(i+height-1);
						stack.push(i+height-1);
						matrix[i+height-1] = -1;
					}
				}
				if (h != height-1){
					if (matrix[i+height+1] == 0) {
						result.add(i+height+1);
						stack.push(i+height+1);
						matrix[i+height+1] = -1;
					}
				}
			}
			if (h != 0) {
				if (matrix[i-1] == 0) {
					result.add(i-1);
					stack.push(i-1);
					matrix[i-1] = -1;
				}
			}
			if (h != height-1){
				if (matrix[i+1] == 0) {
					result.add(i+1);
					stack.push(i+1);
					matrix[i+1] = -1;
				}
			}
		}
		return result;
	}
	
	public static void openSmooth(int[] matrix, int width, int height, int iters) {
		int[] newmatrix = (int[])matrix.clone();
		int[] newmatrix2 = (int[])matrix.clone();
		for (int z = 0; z < iters; z++) {
			for (int i = 0; i < width; i++){
				for (int j = 0; j < height; j++){
					if (newmatrix2[i*height+j] == 0) {
						if (i != 0) {
							newmatrix[(i-1)*height+j] = 0;
							if (j != 0) {
								newmatrix[(i-1)*height+(j-1)] = 0;
							}
							if (j != height-1){
								newmatrix[(i-1)*height+(j+1)] = 0;
							}
						}
						if (i != width-1) {
							newmatrix[(i+1)*height+(j)] = 0;
							if (j != 0) {
								newmatrix[(i+1)*height+(j-1)] = 0;
							}
							if (j != height-1){
								newmatrix[(i+1)*height+(j+1)] = 0;
							}
						}
						if (j != 0) {
							newmatrix[(i)*height+(j-1)] = 0;
						}
						if (j != height-1){
							newmatrix[(i)*height+(j+1)] = 0;
						}
					}
				}
			}
			newmatrix2 = (int[])newmatrix.clone();
		}
		for (int z = 0; z < iters; z++) {
			for (int i = 0; i < width; i++){
				for (int j = 0; j < height; j++){
					if (newmatrix2[i*height+j] == 1) {
						if (i != 0) {
							newmatrix[(i-1)*height+j] = 1;
							if (j != 0) {
								newmatrix[(i-1)*height+(j-1)] = 1;
							}
							if (j != height-1){
								newmatrix[(i-1)*height+(j+1)] = 1;
							}
						}
						if (i != width-1) {
							newmatrix[(i+1)*height+(j)] = 1;
							if (j != 0) {
								newmatrix[(i+1)*height+(j-1)] = 1;
							}
							if (j != height-1){
								newmatrix[(i+1)*height+(j+1)] = 1;
							}
						}
						if (j != 0) {
							newmatrix[(i)*height+(j-1)] = 1;
						}
						if (j != height-1){
							newmatrix[(i)*height+(j+1)] = 1;
						}
					}
				}
			}
			newmatrix2 = (int[])newmatrix.clone();
		}
		for (int i = 0; i < width*height; i++){
			matrix[i] = newmatrix2[i];
		}
	}
	
	public static List<Integer> getBoundaryPixels(int[] matrix, int width, int height) {
		List<Integer> result = new ArrayList<Integer>();
		for (int i = 0; i < width; i++){
			for (int j = 0; j < height; j++){
				if (matrix[i*height+j] == 1) {
					if (i != 0) {
						if (matrix[(i-1)*height+j] == 0) {
							result.add(i*height+j);
							continue;
						}
						if (j != 0) {
							if (matrix[(i-1)*height+(j-1)] == 0){
								result.add(i*height+j);
								continue;
							}
						}
						if (j != height-1){
							if (matrix[(i-1)*height+(j+1)] == 0){
								result.add(i*height+j);
								continue;
							}
						}
					}
					if (i != width-1) {
						if (matrix[(i+1)*height+(j)] == 0){
							result.add(i*height+j);
							continue;
						}
						if (j != 0) {
							if (matrix[(i+1)*height+(j-1)] == 0){
								result.add(i*height+j);
								continue;
							}
						}
						if (j != height-1){
							if (matrix[(i+1)*height+(j+1)] == 0){
								result.add(i*height+j);
								continue;
							}
						}
					}
					if (j != 0) {
						if (matrix[(i)*height+(j-1)] == 0){
							result.add(i*height+j);
							continue;
						}
					}
					if (j != height-1){
						if (matrix[(i)*height+(j+1)] == 0){
							result.add(i*height+j);
							continue;
						}
					}
				}
			}
		}
		return result;
	}
}
