package app.tools.utils;

import java.io.BufferedInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import app.data.Bin;
import app.tools.structure.Landmark;
import app.tools.structure.Mesh;
import app.tools.utils.FileUtils;
import app.tools.utils.MeshLoaderUtils;
import app.tools.utils.OSValidator;

/**
 * Wrapper class for external registration executable. Static methods are used to begin and access a registration session, which passes
 * landmark and mesh data to files which are in turn used to build and solve the linear system of equations to fit mesh to landmarks.
 *
 * @author Adam Steffen
 */
public class RegistrationUtils {
	
	//-------------------------------------------------------------------
	// Static Variables/Files
	//-------------------------------------------------------------------
	
	private static File MESH_FILE_BACKUP;
	private static File MESH_FILE;
	private static File MASK_FILE;
	private static File LANDMARK_FILE;
	private static File ATA_FILE_BACKUP;
	private static File ATB_FILE_BACKUP;
	private static File ATA_FILE;
	private static File ATB_FILE;
	
	private static File registrationExe;
	
	private static Integer subdivisionLevel;
	private static boolean precomputed;
	
	//-------------------------------------------------------------------
	// Public Methods
	//-------------------------------------------------------------------
	
	/**
	 * Initialize a new global Registration session. All previous session info is lost
	 * @param model Mesh state at the beginning of this session
	 * @param landmarks Initial added to fitting matrices, can be empty but not null
	 * @param subLevel Set the subdivision level for this session
	 * @return True if the session can create and write to the static files, False if any errors occur
	 */
	public static boolean startRegistrationSession(Mesh model, Integer subLevel) {
		try {
			MESH_FILE_BACKUP = File.createTempFile("temp_mesh_backup", ".pol");
			MESH_FILE = File.createTempFile("temp_mesh",".pol");
			MASK_FILE = File.createTempFile("temp_mask",".pol");
			LANDMARK_FILE = File.createTempFile("temp_landmark",".pol");
			ATA_FILE_BACKUP = File.createTempFile("temp_ata_backup",".pol");
			ATB_FILE_BACKUP = File.createTempFile("temp_atb_backup",".pol");
			ATA_FILE = File.createTempFile("temp_ata",".pol");
			ATB_FILE = File.createTempFile("temp_atb",".pol");
			
			registrationExe = getRegisterExe("OSX/Register3D");
			if (OSValidator.isWindows()) {
				registrationExe = getRegisterExe("WIN64\\registerAll2");
			}
			registrationExe.setExecutable(true); 
			
			precomputed = false;
			subdivisionLevel = subLevel;
			clearFiles();
			if (model == null) return true;
			MeshLoaderUtils.writeMesh(model, MESH_FILE_BACKUP);
			FileUtils.copyfile(MESH_FILE_BACKUP,MESH_FILE);
			subdivideMask();
		} catch (IOException e) {
			return false;
		}
		return true;
	}
	
	/**
	 * Revert to last save point, resetting the mesh and all matrices.
	 * @return Mesh as last save point, or <b>null</b> if errors occur with file IO
	 */
	public static Mesh resetRegistrationSession() {
		if (MESH_FILE_BACKUP == null) return null;
		FileUtils.copyfile(MESH_FILE_BACKUP,MESH_FILE);
		if (ATA_FILE_BACKUP.exists()) {
			FileUtils.copyfile(ATA_FILE_BACKUP,ATA_FILE);
		}
		if (ATB_FILE_BACKUP.exists()) {
			FileUtils.copyfile(ATB_FILE_BACKUP,ATB_FILE);
		}
		try {
			return MeshLoaderUtils.loadMesh(MESH_FILE_BACKUP);
		} catch (Exception e) {
			return null;
		}
	}
	
	/**
	 * Progress the save point for iterative registration to the current mesh. After calling this method previous registration
	 * steps cannot be undone.
	 * @return Current mesh save point, or <b>null</b> if there are problems with file IO
	 */
	public static Mesh saveRegistrationSession() {
		FileUtils.copyfile(MESH_FILE,MESH_FILE_BACKUP);
		if (ATA_FILE.exists()) {
			FileUtils.copyfile(ATA_FILE,ATA_FILE_BACKUP);
		}
		if (ATB_FILE.exists()) {
			FileUtils.copyfile(ATB_FILE,ATB_FILE_BACKUP);
		}
		try {
			return MeshLoaderUtils.loadMesh(MESH_FILE_BACKUP);
		} catch (Exception e) {
			return null;
		}
	}
	
	/**
	 * Progress the save point for iterative registration to the current mesh. After calling this method previous registration
	 * steps cannot be undone.
	 * @param mesh Mesh state of this session
	 * @return Current mesh save point, or <b>null</b> if there are problems with file IO
	 * @throws IOException 
	 */
	public static void saveRegistrationSession(Mesh mesh) throws IOException {
		MeshLoaderUtils.writeMesh(mesh, MESH_FILE_BACKUP);
		FileUtils.copyfile(MESH_FILE_BACKUP,MESH_FILE);
		if (ATA_FILE.exists()) {
			FileUtils.copyfile(ATA_FILE,ATA_FILE_BACKUP);
		}
		if (ATB_FILE.exists()) {
			FileUtils.copyfile(ATB_FILE,ATB_FILE_BACKUP);
		}
	}
	
	/**
	 * Add additional landmarks to the fitting matrices and iteratively solve those matrices
	 * @param landmarks New landmarks to add
	 * @param fit Fitting weight
	 * @param deform Deformation weight
	 * @param iters Iterations of matrix solves
	 * @return Resultant mesh is return, or null returned if errors occur in the IO
	 */
	public static Mesh addLandmarksToFit(List<Landmark> landmarks, Double fit, Double deform, Integer iters) {

		if (!precomputed) {
			try {
				if (landmarks.isEmpty()) {
					return MeshLoaderUtils.loadMesh(MESH_FILE);
				} else {
					precomputeMatrices(landmarks, fit, deform, iters);
					FileUtils.copyfile(ATA_FILE_BACKUP,ATA_FILE);
					FileUtils.copyfile(ATB_FILE_BACKUP,ATB_FILE);
					solveRegistration();
					iters--;
					precomputed = true;
				}
			} catch (IOException e) {
				return null;
			}
		} else {
			try {
				if (landmarks.isEmpty()) {
					return MeshLoaderUtils.loadMesh(MESH_FILE);
				} else {
					MeshLoaderUtils.writeLandmarks(landmarks, LANDMARK_FILE);
				}
			} catch (IOException e) {
				return null;
			}
		}
		
		for (int k =0; k < iters; k++) {
			try {
				Runtime rt = Runtime.getRuntime();
				String[] cmd ={registrationExe.getAbsolutePath(),"addfit","-p",MESH_FILE.getCanonicalPath()
						,"-l",LANDMARK_FILE.getCanonicalPath(),"-s",""+subdivisionLevel,"-i",""+1,"-m",MASK_FILE.getCanonicalPath(),
						"-f",""+fit,"-d",""+deform,"-a",ATA_FILE.getCanonicalPath(),"-b",ATB_FILE.getCanonicalPath()};
				Process pr = rt.exec(cmd);
				pr.waitFor();
			} catch(Exception e) {
				e.printStackTrace();
			}			
			solveRegistration();
		}	
		try {
			return MeshLoaderUtils.loadMesh(MESH_FILE);
		} catch (Exception e) {
			return null;
		}
	}
	
	/**
	 * Locally register along a specified boundary
	 * @param landmarks landmarks to add fit to
	 * @param fit Fitting weight
	 * @param deform Deformation weight
	 * @param iters Iterations of matrix solves
	 * @param partial unregistered mesh
	 * @param activeMaterials materials that specify the boundary to register along
	 * @return mesh after registration
	 * @throws IOException
	 */
	public static Mesh localRegistration(List<Landmark> landmarks, Double fit, Double deform, Integer iters, Mesh partial, List<Integer> activeMaterials) throws IOException {
		if (partial == null) { return null; }
		if (landmarks.isEmpty()) { return null; };
		
		File inputMeshFile = File.createTempFile("input", "pol");
		MeshLoaderUtils.writeMesh(partial, inputMeshFile);
		
		List<Landmark> fullLandmarks = partial.getSurfaceLandmarks(subdivisionLevel);
		fullLandmarks.addAll(landmarks);
		File landmarkFile = File.createTempFile("landmarks", "pol");
		MeshLoaderUtils.writeLandmarks(fullLandmarks, landmarkFile);	
		
		File resultMeshFile = File.createTempFile("output", "pol");
		try {
			Runtime rt = Runtime.getRuntime();
			String[] cmd ={registrationExe.getAbsolutePath(),"full","-p",inputMeshFile.getCanonicalPath()
					,"-l",landmarkFile.getCanonicalPath(),"-s",""+subdivisionLevel,"-i",""+iters,
					" -f ",""+fit,"-d",""+deform,"-o",resultMeshFile.getAbsolutePath()};		
			Process pr = rt.exec(cmd);
			pr.waitFor();
		} catch(Exception e) {
			e.printStackTrace();
		}
		return MeshLoaderUtils.loadMesh(resultMeshFile);
	}
	
	//-------------------------------------------------------------------
	// Helper Methods
	//-------------------------------------------------------------------
	
	/**
	 * Remove all static files
	 */
	private static void clearFiles() {
		precomputed = false;
		MESH_FILE.delete();
		MASK_FILE.delete();
		ATA_FILE.delete();
		ATB_FILE.delete();
		LANDMARK_FILE.delete();		
		MESH_FILE_BACKUP.delete();
		ATA_FILE_BACKUP.delete();
		ATB_FILE_BACKUP.delete();
	}
	
	/**
	 * Create the subdivision mask matrix m
	 */
	private static void subdivideMask() {
		try {
			Runtime rt = Runtime.getRuntime();		
			String[] cmd ={registrationExe.getAbsolutePath(),"submask","-p",MESH_FILE_BACKUP.getCanonicalPath(),
						"-s",""+subdivisionLevel,"-m",MASK_FILE.getCanonicalPath()};
			Process pr = rt.exec(cmd);
			pr.waitFor();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Generate the initial AtA and AtB matrices for the first landmarks added to the fit
	 */
	private static void precomputeMatrices(final List<Landmark> landmarks, Double fit, Double deform, Integer iters) {
		try {
			MeshLoaderUtils.writeLandmarks(landmarks, LANDMARK_FILE);		
			Runtime rt = Runtime.getRuntime();
			String[] cmd ={registrationExe.getAbsolutePath(),"precompute","-p",MESH_FILE_BACKUP.getCanonicalPath()
					,"-l",LANDMARK_FILE.getCanonicalPath(),"-s",""+subdivisionLevel,"-i",""+iters,"-m",MASK_FILE.getCanonicalPath(),
					" -f ",""+fit,"-d",""+deform,"-a",ATA_FILE_BACKUP.getCanonicalPath(),"-b",ATB_FILE_BACKUP.getCanonicalPath()};		
			Process pr = rt.exec(cmd);
			pr.waitFor();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Solve the AtA*m = AtB linear system
	 */
	private static void solveRegistration() {	
		try {
			Runtime rt = Runtime.getRuntime();
			String[] cmd ={registrationExe.getAbsolutePath(),"solve","-p",MESH_FILE.getCanonicalPath(),
					"-a",ATA_FILE.getCanonicalPath(),"-b",ATB_FILE.getCanonicalPath(),"-o",MESH_FILE.getCanonicalPath()};
			Process pr = rt.exec(cmd);
			pr.waitFor();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Create a temporary executable file for use by the application
	 */
	private static File getRegisterExe(String path) {
		try {
			InputStream in = new BufferedInputStream(Bin.getResourceClass().getResourceAsStream(path));
			
			File exeFile = File.createTempFile("Register3D", null);
			
			DataOutputStream writer = new DataOutputStream(new FileOutputStream(exeFile));

			long oneChar = 0;
			while((oneChar = in.read()) != -1){
				writer.write((int)oneChar);
			}

			in.close();
			writer.close();
			return exeFile;
		} catch (Exception e) {
			return null;
		} 
	}
}
