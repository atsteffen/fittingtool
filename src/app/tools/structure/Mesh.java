package app.tools.structure;

import java.awt.Color;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import app.tools.topology.Edge;
import app.tools.topology.Face;
import app.tools.topology.Octahedron;
import app.tools.topology.Polyhedron;
import app.tools.topology.Tetrahedron;
import app.tools.topology.Topology;
import app.tools.topology.Vertex;
import app.tools.utils.Log;
import app.tools.utils.MeshLoaderUtils;

public class Mesh implements Cloneable {
	private Topology topology;
	private Geometry geometry;
	private Vertex centroid;
	private int subDivLevel;

	public static final int VIEW_ID_1 = 0, VIEW_ID_2 = 1;

	//******************************************************************************************************************************
	//
	//			CONSTRUCTION & INITIALIZATION
	//
	//******************************************************************************************************************************

	public Mesh (Topology t, Geometry g) {
		topology = t;
		geometry = g;
		subDivLevel = 0;
		init();
	}

	private void init() {
		//Initialize centroid
		centroid = new Vertex(0,0,0);
		for (int i = 0; i < geometry.size(); ++i) {
			centroid.setX(geometry.get(i).getX() + centroid.getX());
			centroid.setY(geometry.get(i).getY() + centroid.getY());
			centroid.setZ(geometry.get(i).getZ() + centroid.getZ());
		}
		centroid.setX(centroid.getX()/geometry.size());
		centroid.setY(centroid.getY()/geometry.size());
		centroid.setZ(centroid.getZ()/geometry.size());
	}
	
	//******************************************************************************************************************************
	//
	//			ACCESSORS & MUTATORS
	//
	//******************************************************************************************************************************

	public int getSubDivLevel() { return subDivLevel; }

	public Topology getTopology() {
		return topology;
	}

	public Geometry getGeometry() {
		return geometry;
	}

	public Vertex getCentroid() {
		return centroid;
	}

	public ArrayList<Integer> getPartitionIDs() {
		HashSet<Integer> partitionIDs = new HashSet<Integer>();
		for (Polyhedron p : topology.getPolyhedra()) 
			partitionIDs.add(p.getMaterial());

		ArrayList<Integer> result = new ArrayList<Integer>();
		result.addAll(partitionIDs);
		return result;
	}

	//******************************************************************************************************************************
	//
	//			STATIC FUNCTIONALITY (e.g. component coloring)
	//
	//******************************************************************************************************************************

	/** Colors and values used to represent partitions. 
	 * NOTE: Color.DARK_GRAY is reserved for faces bounding "the void." 
	 */
	public final static int NULL_BOUNDARY = -1;
	/** Should represent the MAXIMUM number of vertices possible in any instance of the Mesh class. */
	public static final int DEFAULT_BUFFER = 1000000;

	public static boolean showHigh = true, showMod = true, showLow = true, showNone = true, showUnselected = false;;

	public static Color getColor(int material) { 
		if (material == NULL_BOUNDARY)
			return Regions.getColor(Regions.getDefaultIndex());
		//return submeshColors[material%submeshColors.length]; 
		return Regions.getColor(material);
	}

	public static String getName(int material) {
		if (material == NULL_BOUNDARY) 
			return "Null Region";
		else 
			return Regions.getName(material);
	}

	/** Sets visibility of unselected polyhedra.
	 * @param visible
	 */
	public static void setShown(boolean visible) {
			showUnselected = visible;
	}

	/** Tells whether or not polyhedra having a particular property should be shown.
	 * Display of unselected polyhedra is (or should be) subject to the other four rules.
	 * 
	 * @param property
	 * @return shown
	 */
	public static boolean getShown(int property) {
		switch (property) {
		case 3:
			return showHigh;
		case 2:
			return showMod;
		case 1:
			return showLow;
		case 0:
			return showNone;
		case -1:
			return showUnselected;
		default:
			Log.log("Unrecognized property type in Mesh's getShown() method.");
			return false;
		}
	}

	//******************************************************************************************************************************
	//
	//			SUBDIVISION
	//
	//******************************************************************************************************************************
	/** Original vertex weight */ private static float TET_WEIGHT_1 = -1.0f/16.0f;
	/** Adjacent vertex weight*/ private static float TET_WEIGHT_2 = 17.0f/48.0f;
	/** Original vertex weight */ private static float OCT_WEIGHT_1 = 3.0f/8.0f;
	/** Adjacent vertex weight*/ private static float OCT_WEIGHT_2 = 1.0f/12.0f;
	/** Opposite vertex weight*/ private static float OCT_WEIGHT_3 = 7.0f/24.0f;

	public void subdivide() {
		linearSubdivision();
		smoothing();
		subDivLevel++;
	}

	/** Splits each element of the mesh:
	 * 1 Tetrahedron -> 4 Tetrahedron + 1 Octahedron
	 * 1 Octahedron -> 8 Tetrahedron + 6 Octahedron
	 * 1 Crease Edge -> 2 Crease Edges
	 * 1 Crease Face -> 4 Crease Faces
	 * 1 Crease Vertex -> 1 Crease Vertex
	 */
	private void linearSubdivision() {
		GeometryMap hash = new GeometryMap(geometry);

		Topology oldTopology = topology;
		topology = new Topology(oldTopology.getPoints(), new ArrayList<Edge>(), new ArrayList<Face>(), new ArrayList<Polyhedron>());

		for (Polyhedron p : oldTopology.getPolyhedra()) 
			topology.getPolyhedra().addAll(p.subdivide(geometry, hash));
		for (Face f : oldTopology.getFaces())
			topology.getFaces().addAll(f.subdivide(geometry, hash));
		for (Edge e : oldTopology.getEdges()) 
			topology.getEdges().addAll(e.subdivide(geometry, hash));
	}
	/** Smooths all elements */ 
	private void smoothing() {
		short[] degree = initDegrees();
		int[] valence = initValences();

		Vertex[] refVertices = new Vertex[geometry.getPoints().size()];
		for (int i = 0; i < geometry.getPoints().size(); ++i)  {
			refVertices[i] = new Vertex(geometry.get(i).getX(),geometry.get(i).getY(),geometry.get(i).getZ() );
			geometry.get(i).setXYZ(new float[] {0f,0f,0f});	
		}

		smoothPoints(valence, degree, refVertices);
		smoothEdges(valence, degree, refVertices);
		smoothFaces(valence, degree, refVertices);
		smoothPolyhedra(valence, degree, refVertices);

		for (int i = 0; i < geometry.size(); ++i) {
			Vertex v = geometry.get(i);	
			v.setXYZ(new float[]{	v.getX()/valence[i],		v.getY()/valence[i],			v.getZ()/valence[i]			});
		}
	}

	/** Smooth crease-points */ 			private void smoothPoints(int[] valence, short[] degree, Vertex[] ref) {
		for (Integer i : topology.getPoints())  {
			if (degree[i] == Vertex.CREASE_POINT) {
				geometry.get(i).setXYZ(ref[i].getXYZ());
				valence[i] = 1;
			} else { Log.log("Error: Invalid crease-point in smoothing step."); }
		}
	}
	/** Smooth crease-edge points*/ 	private void smoothEdges(int[] valence, short[] degree, Vertex[] ref) {
		for (Edge e : topology.getEdges()) {
			Vertex a = ref[e.getVertices()[0]];			
			Vertex b = ref[e.getVertices()[1]];
			Vertex mid = Vertex.midpoint(a, b);

			for (int i = 0; i < e.getVertices().length; ++i) {
				if ( degree[e.getVertices()[i]] == Vertex.CREASE_EDGE){
					Vertex cur = geometry.get(e.getVertices()[i]);
					cur.setXYZ(new float[]{	cur.getX()+mid.getX(),		 cur.getY()+mid.getY(), 		cur.getZ()+mid.getZ()});
					valence[e.getVertices()[i]]++;
				}
			}
		}
	}
	/** Smooth crease-face points */	 	private void smoothFaces(int[] valence, short[] degree, Vertex[] ref) {
		for (Face f : topology.getFaces()) 	//Pre-compute valences for crease-face vertices
			for (int i : f.getPoints()) 
				if (degree[i] == Vertex.CREASE_FACE) 
					valence[i]++;

		for (Face f : topology.getFaces()) {
			for (int i = 0; i < f.getPoints().length; ++i) 
				if (degree[f.getPoints()[i]] == Vertex.CREASE_FACE) {	
					int j = f.getPoints()[i];

					float w = (5f/8f) - (float)Math.pow((3f/8f) + (1f/4f)*
							Math.cos(2*Math.PI/valence[j]), 2);

					Vertex a = ref[f.getPoints()[i]				].getScaled(1-2*w);
					Vertex b = ref[f.getPoints()[(i+1)%3]	].getScaled(w);
					Vertex c = ref[f.getPoints()[(i+2)%3]	].getScaled(w);

					Vertex loopCentroid = new Vertex(		
							(a.getX()+b.getX()+c.getX()), 
							(a.getY()+b.getY()+c.getY()),
							(a.getZ()+b.getZ()+c.getZ())
					);

					geometry.get(j).setX(geometry.get(j).getX() + loopCentroid.getX());
					geometry.get(j).setY(geometry.get(j).getY() + loopCentroid.getY());
					geometry.get(j).setZ(geometry.get(j).getZ() + loopCentroid.getZ());

				} else if (degree[f.getPoints()[i]] > Vertex.CREASE_FACE) {
					Log.log("ERROR: Vertex of degree " + degree[i] + " found in a crease face.");
				}
		}
	}
	/** Smooth all other points */			private void smoothPolyhedra(int[] valence, short[] degree, Vertex[] ref) {
		for (Polyhedron p : topology.getPolyhedra()) {
			if (p instanceof Tetrahedron) 
				smoothTetrahedron((Tetrahedron)p, valence, degree, ref);
			else
				smoothOctahedron((Octahedron)p, valence, degree, ref);
		}
	}

	private void smoothTetrahedron(Tetrahedron poly, int[] valence, short[] degree, Vertex[] ref) {
		for (int index = 0; index < poly.getVertices().length; ++index) {
			if (degree[poly.getVertices()[index]] != Vertex.NORMAL)
				continue;

			Vertex target = geometry.get(poly.getVertices()[index]);

			for (int i = 0; i < poly.getVertices().length; ++i) {
				Vertex current = ref[poly.getVertices()[i]];
				if (i == index) {
					target.setX(		target.getX() + TET_WEIGHT_1*current.getX() 		);
					target.setY(		target.getY() + TET_WEIGHT_1*current.getY() 		);
					target.setZ(		target.getZ() + TET_WEIGHT_1*current.getZ() 		);
				}
				else {
					target.setX(		target.getX() + TET_WEIGHT_2*current.getX() 		);
					target.setY(		target.getY() + TET_WEIGHT_2*current.getY() 		);
					target.setZ(		target.getZ() + TET_WEIGHT_2*current.getZ() 		);
				}	
			}

			valence[poly.getVertices()[index]]++;
		}
	}
	private void smoothOctahedron(Octahedron poly, int[] valence, short[] degree, Vertex[] ref) {
		for (int index = 0; index < poly.getVertices().length; ++index) {
			if (degree[poly.getVertices()[index]] != Vertex.NORMAL)
				continue;

			int offset = 1;
			if (index%2 == 1)
				offset = -1;

			Vertex target = geometry.get(poly.getVertices()[index]);

			for (int i = 0; i < poly.getVertices().length; ++i) {
				Vertex current = ref[poly.getVertices()[i]];

				if (i == index) {
					target.setX(		target.getX() + OCT_WEIGHT_1*current.getX() 		);
					target.setY(		target.getY() + OCT_WEIGHT_1*current.getY() 		);
					target.setZ(		target.getZ() + OCT_WEIGHT_1*current.getZ() 			);
				}
				else if (i == index + offset) {
					target.setX(		target.getX() + OCT_WEIGHT_3*current.getX() 		);
					target.setY(		target.getY() + OCT_WEIGHT_3*current.getY() 		);
					target.setZ(		target.getZ() + OCT_WEIGHT_3*current.getZ() 			);
				}
				else {
					target.setX(		target.getX() + OCT_WEIGHT_2*current.getX() 		);
					target.setY(		target.getY() + OCT_WEIGHT_2*current.getY() 		);
					target.setZ(		target.getZ() + OCT_WEIGHT_2*current.getZ() 			);
				}	
			}

			valence[poly.getVertices()[index]]++;

		}
	}

	private short[] initDegrees() {
		short[] degrees = new short[geometry.size()];

		for (int i = 0; i < geometry.size(); ++i)
			degrees[i] = Vertex.NORMAL;

		for (Face f : topology.getFaces()) 
			for (int index : f.getPoints()) 
				degrees[index] = Vertex.CREASE_FACE;

		for (Edge e : topology.getEdges()) 
			for (int index : e.getVertices()) 
				degrees[index] = Vertex.CREASE_EDGE;

		for (Integer index : topology.getPoints())
			degrees[index] = Vertex.CREASE_POINT;

		return degrees;
	}
	private int[] initValences() {
		int[] valence = new int[geometry.getPoints().size()];
		for (int i = 0; i < valence.length; ++i)
			valence[i] = 0;
		return valence;
	}

	public void pcaAlignToLandmarks(List<Landmark> landmarks, Mesh baseMesh) {
		
		Vertex centerM = new Vertex(0.0f,0.0f,0.0f);
		float maxXMesh = Float.MIN_VALUE;
		float maxYMesh = Float.MIN_VALUE;
		float maxZMesh = Float.MIN_VALUE;
		float minXMesh = Float.MAX_VALUE;
		float minYMesh = Float.MAX_VALUE;
		float minZMesh = Float.MAX_VALUE;
		
		for (int i = 0; i < geometry.size(); ++i) {
			maxXMesh = Math.max(maxXMesh,geometry.get(i).getX());
			maxYMesh = Math.max(maxYMesh,geometry.get(i).getY());
			maxZMesh = Math.max(maxZMesh,geometry.get(i).getZ());
			minXMesh = Math.min(minXMesh,geometry.get(i).getX());
			minYMesh = Math.min(minYMesh,geometry.get(i).getY());
			minZMesh = Math.min(minZMesh,geometry.get(i).getZ());
		}
		centerM.setX((maxXMesh-minXMesh)/2 + minXMesh);
		centerM.setY((maxYMesh-minYMesh)/2 + minYMesh);
		centerM.setZ((maxZMesh-minZMesh)/2 + minZMesh);
		
		Vertex centerL = new Vertex(0.0f,0.0f,0.0f);
		float maxXLandmark = Float.MIN_VALUE;
		float maxYLandmark = Float.MIN_VALUE;
		float maxZLandmark = Float.MIN_VALUE;
		float minXLandmark = Float.MAX_VALUE;
		float minYLandmark = Float.MAX_VALUE;
		float minZLandmark = Float.MAX_VALUE;
		for (int i = 0; i < landmarks.size(); ++i){
			maxXLandmark = Math.max(maxXLandmark,landmarks.get(i).getLocation().getX());
			maxYLandmark = Math.max(maxYLandmark,landmarks.get(i).getLocation().getY());
			maxZLandmark = Math.max(maxZLandmark,landmarks.get(i).getLocation().getZ());
			minXLandmark = Math.min(minXLandmark,landmarks.get(i).getLocation().getX());
			minYLandmark = Math.min(minYLandmark,landmarks.get(i).getLocation().getY());
			minZLandmark = Math.min(minZLandmark,landmarks.get(i).getLocation().getZ());
		}
		centerL.setX((maxXLandmark-minXLandmark)/2 + minXLandmark);
		centerL.setY((maxYLandmark-minYLandmark)/2 + minYLandmark);
		centerL.setZ((maxZLandmark-minZLandmark)/2 + minZLandmark);
		
		float deltaXLandmark = maxXLandmark - minXLandmark;
		float deltaYLandmark = maxYLandmark - minYLandmark;
		float deltaZLandmark = maxZLandmark - minZLandmark;
		
		float deltaXMesh = maxXMesh - minXMesh;
		float deltaYMesh = maxYMesh - minYMesh;
		float deltaZMesh = maxZMesh - minZMesh;
		
		float scaleX = deltaXMesh/deltaXLandmark;
		float scaleY = deltaYMesh/deltaYLandmark;
		float scaleZ = deltaZMesh/deltaZLandmark;
		
		for (int i = 0; i < baseMesh.getGeometry().size(); ++i) {
			Vertex vertexMesh = baseMesh.getGeometry().get(i).plus(new Vertex(0,0,0).minus(centerM));
			vertexMesh.setX(vertexMesh.getX()/scaleX);
			vertexMesh.setY(vertexMesh.getY()/scaleY);
			vertexMesh.setZ(vertexMesh.getZ()/scaleZ);
			baseMesh.getGeometry().set(i, vertexMesh.plus(centerM.minus(new Vertex(0,0,0))));
		}
		
		// align centroids
		for (int i = 0; i < baseMesh.getGeometry().size(); ++i) {
			baseMesh.getGeometry().set(i,baseMesh.getGeometry().get(i).plus(centerL.minus(centerM)));
		}	
	}
	
	@Override
	public Object clone() {
		try {
			Mesh result = (Mesh) super.clone();
			result.topology = (Topology) result.topology.clone();
			result.geometry = (Geometry) result.geometry.clone();
			result.centroid = (Vertex) result.centroid.clone();
			return result;
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
			return null;
		}	
	}

	public void setPartialMesh(Map<Integer,Integer> pointMap, Mesh partial, List<Integer> activeMaterials) {
		Set<Integer> points = new HashSet<Integer>();
		ArrayList<Integer> array = new ArrayList<Integer>(pointMap.keySet());
		for (Edge e : partial.getTopology().getEdges()) {
			if (e.getMaterials().contains(Mesh.NULL_BOUNDARY)) {
				for (Integer i : e.getVertices()) {
					points.add(i);
				}
			}
		}
		for (Face f : partial.getTopology().getFaces()) {
			if ( ( f.getMaterial(true) == activeMaterials.get(0) && f.getMaterial(false) == activeMaterials.get(1) )				
					|| ( f.getMaterial(false) == activeMaterials.get(0) && f.getMaterial(true) == activeMaterials.get(1) ) ) {
				for (Integer i : f.getPoints()) {
					if (points.add(i)) {
						getGeometry().set(array.get(i), partial.getGeometry().get(i));
					}
				}
			}
		}
	}

	public Mesh getPartialMesh(final Map<Integer,Integer> partialVertMap, List<Integer> activeMaterials) {
		
		if (activeMaterials.contains(-2)) return null;
		
		List<Edge> edges = new ArrayList<Edge>();
		List<Face> faces = new ArrayList<Face>();
		List<Polyhedron> polys = new ArrayList<Polyhedron>();
		List<Vertex> verts = new ArrayList<Vertex>();
		
		int indexCount = 0;
		for (Polyhedron p : ((Topology)getTopology().clone()).getPolyhedra()) {
			if (p.getMaterial() == activeMaterials.get(0) || p.getMaterial() == activeMaterials.get(1)) {
				int[] adjusted = new int[p.getVertices().length];
				int localCount = 0;
				for (int i : p.getVertices()) {
					if (partialVertMap.containsKey(i)) {
						adjusted[localCount] = partialVertMap.get(i);
					} else {
						partialVertMap.put(i, indexCount);
						adjusted[localCount] = indexCount;
						indexCount++;
					}
					localCount++;
				}
				if (adjusted.length == 4) {
					polys.add(new Tetrahedron(adjusted, p.getMaterial()));
				} else {
					polys.add(new Octahedron(adjusted, p.getMaterial()));
				}
			}
		}
		for (int i : partialVertMap.keySet()) {
			verts.add(getGeometry().get(i));
		}
		Geometry g = new Geometry(verts);
		faces = MeshLoaderUtils.loadCreaseFaces(g, polys);
		edges = MeshLoaderUtils.loadCreaseEdges(faces);
		List<Integer> points = MeshLoaderUtils.loadCreasePoints(edges);
		return new Mesh(new Topology(points, edges, faces, polys), g);
	}
	
	/**
	 * Get the surface point of a mesh at a specified subdivision level and return them as a list of landmarks
	 * @param subdivisionLevel Number of times to subdivide the mesh before extracting the surface points
	 * @return list of landmarks
	 * @throws IOException 
	 */
	public List<Landmark> getSurfaceLandmarks(int subdivisionLevel) throws IOException {
		Mesh landmarkFarm = (Mesh) this.clone();
		for (int i = 0; i < subdivisionLevel; ++i) {
			landmarkFarm.subdivide();
		}
		Map<Integer,Landmark> newLandmarks = new HashMap<Integer, Landmark>();
		for (Face f : landmarkFarm.getTopology().getFaces()) {
			if (f.contains(Mesh.NULL_BOUNDARY)) {
				for (int i : f.getPoints()) {
					if (!newLandmarks.containsKey(i)) {
						Landmark lm = new Landmark(landmarkFarm.getGeometry().get(i));
						lm.addMaterial(f.getMaterial(true));
						lm.addMaterial(f.getMaterial(false));
						newLandmarks.put(i, lm);
					}
				}
			}
		}
		return new ArrayList<Landmark>(newLandmarks.values());
	}

}
