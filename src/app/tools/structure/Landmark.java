package app.tools.structure;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import app.tools.topology.Vertex;

/**
 * The landmark class used for fitting vertices on the mesh. Made up of a vertex location and a set of materials + display color
 * 
 * @author Adam Steffen
 */
public class Landmark {
	
	private List<Integer> materials;
	private Vertex location;
	private Color displayColor;
	
	/**
	 * Create a new landmark with no materials
	 * @param v location of new landmark
	 */
	public Landmark(Vertex v) {
		location = new Vertex(v.getX(),v.getY(),v.getZ());
		materials = new ArrayList<Integer>();
	}
	
	/**
	 * Add a new material to set of materials
	 * @param i
	 */
	public void addMaterial(Integer i) { materials.add(i); }
	
	/**
	 * Move the landmark by a Vector addition
	 * @param v Vector deviation
	 */
	public void move(Vector v) { location.plus(v); }
	
	/**
	 * @param v Location of landmark
	 */
	public void setLocation(Vertex v){
		location = new Vertex(v.getX(),v.getY(),v.getZ());
	}
	
	/**
	 * @return Position of landmark
	 */
	public Vertex getLocation() {
		return new Vertex(location.getX(),location.getY(),location.getZ());
	}
	
	/**
	 * @return Set of materials of this landmark, used to match to material boundaries on the mesh during registration
	 */
	public List<Integer> getMaterials() { return this.materials; }

	/**
	 * @return Color used to display this landmark
	 */
	public Color getDisplayColor() { return displayColor; }

	/**
	 * @param displayColor Color to use to display this landmark
	 */
	public void setDisplayColor(Color displayColor) { this.displayColor = displayColor; }

	/**
	 * Get a landmark from two regions and a location
	 * @param material1
	 * @param material2
	 * @param location
	 * @return
	 */
	public static Landmark getLandmark(int material1, int material2, Vertex location) {
		Landmark lm = new Landmark(location);
		lm.addMaterial(material1);
		lm.addMaterial(material2);
		
		if (material1 == -2) {
			if (material2 == -2 || material2 == -1){
				lm.setDisplayColor(new Color(1.0f, 1.0f, 0.0f));
			} else {
				Color c = Mesh.getColor(material2);
				lm.setDisplayColor(c);
			}
		} else if (material1 == -1) {
			if (material2 == -2 || material2 == -1) {
				lm.setDisplayColor(new Color(1.0f, 1.0f, 0.0f));
			} else {
				Color c = Mesh.getColor(material2);
				lm.setDisplayColor(c);
			}
		} else {
			if (material2 == -2 || material2 == -1) {
				Color c = Mesh.getColor(material1);
				lm.setDisplayColor(c);
			} else {
				int index = (int) (2*java.lang.Math.random());
				Integer mat = material1;
				if (index == 0) {
					mat = material2;
				}
				Color c = Mesh.getColor(mat);
				lm.setDisplayColor(c);
			}
		}
		return lm;
	}
}
