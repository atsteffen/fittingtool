package app.tests;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import org.junit.Test;

import app.tools.structure.Geometry;
import app.tools.structure.Landmark;
import app.tools.structure.Mesh;
import app.tools.topology.Topology;
import app.tools.topology.Vertex;
import app.tools.utils.MeshLoaderUtils;

public class MeshTest {

	private Mesh testTetrahedron;
	
	@Test
	public void testClone() throws IOException {
		testTetrahedron = MeshLoaderUtils.loadMesh(new File("test_data/small3k_mesh_new.pol"));
		Mesh cloneTet = (Mesh) testTetrahedron.clone();
		
		assertNotSame(testTetrahedron, cloneTet);
		
		Geometry testGeometry = testTetrahedron.getGeometry();
		Topology testTopology = testTetrahedron.getTopology();
		Geometry cloneGeometry = cloneTet.getGeometry();
		Topology cloneTopology = cloneTet.getTopology();
		
		assertNotSame(testGeometry, cloneGeometry);
		assertNotSame(testTopology, cloneTopology);
		
		assertEquals(testGeometry.size(), cloneGeometry.size());
		assertEquals(testTopology.getPoints().size(), cloneTopology.getPoints().size());
		assertEquals(testTopology.getFaces().size(), cloneTopology.getFaces().size());
		assertEquals(testTopology.getEdges().size(), cloneTopology.getEdges().size());
		assertEquals(testTopology.getPolyhedra().size(), cloneTopology.getPolyhedra().size());
		
		Iterator<Vertex> cloneVerticesIterator = cloneGeometry.getPoints().iterator();
		for (Vertex v : testGeometry.getPoints()) {
			Vertex vClone = cloneVerticesIterator.next();
			assertNotSame(v, vClone);
			assertEquals(v.getX(), vClone.getX(), 0.0000001);
			assertEquals(v.getY(), vClone.getY(), 0.0000001);
			assertEquals(v.getZ(), vClone.getZ(), 0.0000001);
		}
		
		Iterator<Integer> clonePointsIterator = cloneTopology.getPoints().iterator();
		for (Integer i : testTopology.getPoints()) {
			Integer iClone = clonePointsIterator.next();
			assertEquals(i, iClone);
		}
	}
	
	@Test
	public void testExtractSurfaceLandmarks() throws Exception {
		testTetrahedron = MeshLoaderUtils.loadMesh(new File("test_data/tripleNested.pol"));
		List<Landmark> landmarks = testTetrahedron.getSurfaceLandmarks(1);
		testTetrahedron.subdivide();
		List<Vertex> vertices = testTetrahedron.getGeometry().getPoints();
		MeshLoaderUtils.writeLandmarks(landmarks, new File("surfaceLandmarks.pol"));
		MeshLoaderUtils.writeMesh(testTetrahedron, new File("surface.pol"));
		assertEquals(vertices.size(), landmarks.size());
	}
}
