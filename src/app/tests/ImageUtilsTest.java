package app.tests;

import static org.junit.Assert.*;

import org.junit.Test;

import app.tools.utils.ImageUtils;

public class ImageUtilsTest {

	@Test
	public void testSmooth() {
		int[] matrix = { 0, 0, 0, 0, 0, 0, 0,
						 0, 0, 0, 0, 0, 0, 0,
						 0, 0, 1, 1, 1, 0, 0,
						 0, 0, 1, 0, 1, 0, 0,
						 0, 0, 1, 1, 1, 0, 0,
						 0, 0, 0, 0, 0, 0, 0,
						 0, 0, 0, 0, 0, 0, 0 };
		ImageUtils.openSmooth(matrix, 7, 7, 1);
		assertEquals(matrix[24],1);
	}
}
