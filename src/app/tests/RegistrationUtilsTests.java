package app.tests;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import app.tools.structure.Landmark;
import app.tools.structure.Mesh;
import app.tools.topology.Vertex;
import app.tools.utils.MeshLoaderUtils;
import app.tools.utils.RegistrationUtils;

public class RegistrationUtilsTests {
	
	private Mesh testTetrahedron;
	private Mesh tripleNested;
	private List<Landmark> testLandmarks;
	private List<Landmark> singlePointDiag;

	@Before
	public void setUp() throws FileNotFoundException, IOException {
		testTetrahedron = MeshLoaderUtils.loadMesh(new File("test_data/testTetrahedron.pol"));
		testLandmarks = MeshLoaderUtils.loadLandmarks(new File("test_data/box.landmarks"));
		tripleNested = MeshLoaderUtils.loadMesh(new File("test_data/tripleNested.pol"));
		singlePointDiag = new ArrayList<Landmark>();
		Landmark singleDiag = new Landmark(new Vertex(1,1,1));
		singleDiag.addMaterial(-2);
		singleDiag.addMaterial(-1);
		singlePointDiag.add(singleDiag);
	}
	
	@After
	public void tearDown() {
		RegistrationUtils.resetRegistrationSession();
	}
	
	@Test
	public void testStartNewRegistrationSession() {
		boolean success = RegistrationUtils.startRegistrationSession(testTetrahedron, 1);
		assertTrue(success);
		success = RegistrationUtils.startRegistrationSession(testTetrahedron, 1);
		assertTrue(success);
		Mesh result = RegistrationUtils.resetRegistrationSession();
		assertTrue(result.getCentroid().getX() == testTetrahedron.getCentroid().getX());
	}
	
	@Test
	public void testAddFitting() {
		RegistrationUtils.startRegistrationSession(testTetrahedron, 1);
		Mesh result = RegistrationUtils.addLandmarksToFit(new ArrayList<Landmark>(), 1.0, 1.0, 1);
		assertTrue(result != null);
		assertTrue(result.getCentroid().getX() == testTetrahedron.getCentroid().getX());
		result = RegistrationUtils.addLandmarksToFit(testLandmarks, 1.0, 1.0, 1);
		assertTrue(result != null);
		assertTrue(result.getCentroid().getX() != testTetrahedron.getCentroid().getX());
		float centroidX = result.getCentroid().getX();
		result = RegistrationUtils.addLandmarksToFit(singlePointDiag, 1.0, 1.0, 1);
		assertTrue(result.getCentroid().getX() != centroidX);
	}
	
	@Test
	public void testSaveRegistrationSession() {
		RegistrationUtils.startRegistrationSession(testTetrahedron, 1);
		Mesh result = RegistrationUtils.addLandmarksToFit(new ArrayList<Landmark>(), 1.0, 1.0, 1);
		System.out.println(result.getCentroid().getX());
		assertTrue(result.getCentroid().getX() == testTetrahedron.getCentroid().getX());
		Mesh result2 = RegistrationUtils.saveRegistrationSession();
		assertTrue(result.getCentroid().getX() == result2.getCentroid().getX());
		result2 = RegistrationUtils.addLandmarksToFit(testLandmarks, 1.0, 1.0, 1);
		System.out.println(result2.getCentroid().getX());
		float centroidX = result2.getCentroid().getX();
		assertTrue(result.getCentroid().getX() != result2.getCentroid().getX());
		result2 = RegistrationUtils.resetRegistrationSession();
		assertTrue(result.getCentroid().getX() == result2.getCentroid().getX());
		result2 = RegistrationUtils.addLandmarksToFit(testLandmarks, 1.0, 1.0, 1);
		assertTrue(result != null);
		assertTrue(result.getCentroid().getX() != result2.getCentroid().getX());
		assertTrue(result2.getCentroid().getX() == centroidX);
	}
	
	@Test
	public void testLocalRegistration() throws Exception {
		RegistrationUtils.startRegistrationSession(null, 1);
		Landmark lm = new Landmark(new Vertex(0.5f, 0.5f, 0.5f));
		lm.addMaterial(0);
		lm.addMaterial(1);
		Mesh result = RegistrationUtils.localRegistration(Arrays.asList(lm), 1.0, 1.0, 1, tripleNested, Arrays.asList(0,1));
		assertTrue(result != null);
	}

}
