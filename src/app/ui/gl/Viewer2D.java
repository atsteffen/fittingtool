package app.ui.gl;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.nio.IntBuffer;
import java.util.NoSuchElementException;

import javax.media.opengl.DebugGL;
import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCapabilities;
import javax.media.opengl.glu.GLU;

import com.sun.opengl.util.BufferUtil;

import app.RegistrationTool;
import app.tools.structure.Landmark;
import app.tools.structure.Mesh;
import app.tools.structure.Vector;
import app.tools.topology.Polyhedron;
import app.tools.topology.Vertex;
import app.tools.utils.Log;
import app.ui.controls.GLPanel;
import app.ui.controls.ImageStreamer;

public class Viewer2D extends GLPanel implements MouseListener, MouseMotionListener, MouseWheelListener {

	//******************************************************************************************************************************
	//
	//			CONSTANTS AND MEMBER VARIABLES
	//
	//******************************************************************************************************************************

	/** Default serial version UID  */
	private static final long serialVersionUID = 1L;
	/**Select to affect points */ 				public static final short ELEMENTS_POINTS 			= 1;	
	/**Select to affect edges */ 				public static final short ELEMENTS_EDGES			= 2;	
	/**Select to affect faces */ 				public static final short ELEMENTS_FACES 			= 3;
												public static final short ELEMENTS_IMAGE			= 4;
												
												private static int lastLocX;
												private static int lastLocY;

	private boolean showPoints = true;
	private boolean showColor = false;
	private boolean showImage = true;
	private boolean showEdges = true;
	private boolean isDragging			= false;
	private boolean mouseReleased 	= false;
	private boolean isShiftClick = false;
	private boolean flipSwitch = true;
	private int 		mouseX = 0, mouseOverX = 0, lastMouseOverX = 0;
	private int 		mouseY = 0, mouseOverY = 0, lastMouseOverY = 0;
	private int			screenWidth = 1, screenHeight = 1;
	public float		zoomFactor = 1f, aspectRatio = 1f; // aspectRatio lets us maintain proportions when view is resized.
	private boolean zoomChanged = false;
	private boolean doRefresh		   = true;

	private RegistrationTool app;
	public ImageStreamer streamer;
	public int colorCallList = -1;
	public int blackCallList = -1;

	/** Tells CuttingTool to draw an outline. */ private static final boolean OUTLINE = true;
	/** Tells CuttingTool to draw a filled face*/ private static final boolean FILL = false;

	public static final float LINE_ALPHA = 1f, FILL_ALPHA = 0.5f, VOID_ALPHA = 0.2f;
	public static final float CREASE_LINE_WIDTH = 4f, LINE_WIDTH = 0.2f;						//Note that anti-aliasing affects line width.

	//******************************************************************************************************************************
	//
	//			CONSTRUCTORS AND SUCH
	//
	//******************************************************************************************************************************

	protected Viewer2D(String progName,GLCapabilities glCap) {
		super(progName, glCap);
	}

	public Viewer2D(RegistrationTool app, GLCapabilities glCap) {
		super("Viewer2D",glCap);
		this.app = app;
		this.setBackground(Color.DARK_GRAY);
	}

	/** Runs when the application launches, AND when toggling full-screen mode.
	 * @param gLDrawable
	 */
	public void init(GLAutoDrawable gLDrawable) {
		GL gl = gLDrawable.getGL();
		gLDrawable.setGL( new DebugGL(gLDrawable.getGL()));

		//	gl.glEnable(GL.GL_CULL_FACE);	
		//gl.glShadeModel(GL.GL_SMOOTH);
		gl.glClearColor(1f,1f,1f,0.5f);
		//gl.glClearColor(0.95f, 0.95f, 0.95f, 0.5f);  			 			// Background Color
		gl.glClearDepth(1.0f);                     				 					// Depth Buffer Setup
		gl.glEnable(GL.GL_DEPTH_TEST);             				 			// Enables Depth Testing
		gl.glDepthFunc(GL.GL_LEQUAL);            					   		// The Type Of Depth Testing To Do
		gLDrawable.addKeyListener(this);           	 					// Listening for key events
		gl.glDisable(GL.GL_LIGHTING);

		glCanvas.addMouseMotionListener(this);
		glCanvas.addMouseWheelListener(this);
		glCanvas.addMouseListener(this);

		if ( colorCallList == -1 || blackCallList == -1) {
			colorCallList = gl.glGenLists(1);
			blackCallList = gl.glGenLists(1);
		}

		gl.glEnable(GL.GL_TEXTURE_2D);
		gl.glTexEnvf(GL.GL_TEXTURE_ENV, GL.GL_TEXTURE_ENV_MODE, GL.GL_MODULATE);
	}
	
	public boolean showColor() {
		return showColor;
	}
	
	public void setVisible(short elementType, boolean visible) {
		switch (elementType) {
		case ELEMENTS_POINTS :
			showPoints = visible;
			break;
		case ELEMENTS_EDGES :
			showEdges = visible;
			break;
		case ELEMENTS_FACES :
			showColor = visible;
			break;
		case ELEMENTS_IMAGE :
			showImage = visible;
			break;
		default:
			throw new NoSuchElementException("No type of element matching the input was found. " +
					"Please check provided identifier, elementType=" + elementType + ".");
		}
		refresh();
	}

	/** Generates a new ImageStreamer, given an axis and axisIndex (see below).
	 * 0 => X
	 * 1 => Y
	 * 2 => Z
	 * @param axisIndex
	 */
	public void setImageStreamerAxis(String axis, Vector vec) {
		if (!axis.equals("null") && streamer != null) {
			streamer.setId(axis);
			streamer.setAxis(vec.getNormalized());
		}
	}
	
	public void setStreamer(ImageStreamer streamer) { this.streamer = streamer; }
	public ImageStreamer getStreamer() { return streamer; }

	//******************************************************************************************************************************
	//
	//			MUTATORS & ACCESSORS
	//
	//******************************************************************************************************************************

	public void setPan(int x, int y) {	this.panX = x; this.panY = y;  }
	public void setZoom(float zoom) {	this.zoomFactor = zoom; zoomChanged = true; }

	//******************************************************************************************************************************
	//
	//			GLPANEL-PRESCRIBED METHODS
	//
	//******************************************************************************************************************************



	/** Called by drawable to initiate drawing
	 * @param gLDrawable The GLDrawable Object
	 */
	public void display(GLAutoDrawable gLDrawable) 
	{	
		GL gl = gLDrawable.getGL();
		gl.glLoadIdentity();
		
		// Clear Color Buffer, Depth Buffer
		gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT );
		
		if (app.getMesh() == null) {
			return;
		}
		
		if (zoomChanged || doRefresh) {
			int viewport[] = new int[4];
			gl.glGetIntegerv(GL.GL_VIEWPORT, viewport, 0);
			reshape(gl, (int)(viewport[2]), (int)( viewport[3]));
			zoomChanged = false;
			doRefresh = false;
		} else {	
			pan(gl);
		}
		
		// Mouse Released
		if (!isShiftClick) {
			if (mouseReleased) mouseClick(gl,mouseX,mouseY);
		}

		// Mouse Dragged
		if (isDragging && !isShiftClick) {
			mouseDrag(gl,mouseX,mouseY);
		} else { //Mouse Moved
			mouseOver(gl, mouseOverX, mouseOverY, isShiftClick);
			isShiftClick = false;
			mouseReleased = false;
		}

		// Change Axis
		app.getCutter().rotateView(gl);

		// Update Background Image
		if (streamer != null && showImage) {
			streamer.paintImage(gl, app.getCutter().getP());
		}

		// Draw Outlines
		if (showEdges) updateTriangles(gl);

		// Draw landmarks
		if (showPoints) {
			drawLandmarks(gl);
		}

		mouseReleased = false;

	}

	private void pan(GL gl) {
		float coordWidth = (app.getCutter().getXMax() - app.getCutter().getXMin());
		float coordHeight = (app.getCutter().getYMax() - app.getCutter().getYMin());
		float aspectWidth = (screenWidth/(coordWidth*zoomFactor));
		float aspectHeight = (screenHeight/(coordHeight*zoomFactor));
		float alpha = 0.01f;
		Vector dir = app.getCutter().getN().getNormalized();

		app.getCutter().rotateView(gl);
		if (dir.getX() < alpha && dir.getY() < alpha) {						//Z
			gl.glTranslatef(panX/aspectWidth, panY/aspectHeight,0f);
		} else if (dir.getX() < alpha && dir.getZ() < alpha) {				//Y
			gl.glTranslatef(panX/aspectWidth, 0f,-1f*panY/aspectHeight);
		} else if (dir.getY() < alpha && dir.getZ() < alpha) {				//X
			gl.glTranslatef(0f, panY/aspectHeight,-1f*panX/aspectWidth);
		} else
			Log.log("Unclear which axis this is.");
		app.getCutter().unrotateView(gl);
	}

	@Override
	public void displayChanged(GLAutoDrawable arg0, boolean arg1, boolean arg2) {}

	/** Called by the drawable during the first repaint after the component has 
	 * been resized. The client can update the viewport and view volume of the 
	 * window appropriately, for example by a call to 
	 * GL.glViewport(int, int, int, int); note that for convenience the component
	 * has already called GL.glViewport(int, int, int, int)(x, y, width, height)
	 * when this method is called, so the client may not have to do anything in
	 * this method.
	 * @param gLDrawable The GLDrawable object.
	 * @param x The X Coordinate of the viewport rectangle.
	 * @param y The Y coordinate of the viewport rectangle.
	 * @param width The new width of the window.
	 * @param height The new height of the window.
	 */
	@Override
	public void reshape(GLAutoDrawable gLDrawable, int x, int y, int width, int height) {
		if (app.getMesh() != null) {
			reshape(gLDrawable.getGL(), width, height);
		}
	}

	private void reshape (GL gl, int width, int height) {
		if (height <= 0) // avoid a divide by zero error!
			height = 1;
		screenWidth = width; screenHeight = height;
		aspectRatio = (width*1f/height)  *  (app.getCutter().getYMax()-app.getCutter().getYMin())/(app.getCutter().getXMax()-app.getCutter().getXMin()); 
		if (aspectRatio == 0f) aspectRatio = 0.001f;
		gl.glViewport(0, 0, width, height);
		gl.glMatrixMode(GL.GL_PROJECTION);
		gl.glLoadIdentity();

		gl.glOrtho(
				app.getCutter().getXMin() 		*zoomFactor*aspectRatio, 
				app.getCutter().getXMax()  	*zoomFactor*aspectRatio, 
				app.getCutter().getYMin() *zoomFactor, 
				app.getCutter().getYMax() 	 	*zoomFactor, 
				-250.0f, 250.0f);

		gl.glMatrixMode(GL.GL_MODELVIEW);
		gl.glLoadIdentity();


		if (!zoomChanged) app.getCutter().refresh();

		pan(gl);
	}

	public void refresh() { doRefresh = true; }

	//******************************************************************************************************************************
	//
	//			PAINTING & SELECTION
	//
	//******************************************************************************************************************************

	private void drawLandmarks(GL gl) {
		float pointSize = 4f;
		float scale = 1.0f;
		for (Landmark lm : app.getLandmarks()) {

			// if not currently highlighted boundary continue
			Vector v = app.getCutter().getN();
			// check which axis we are on
			Vertex p = app.getCutter().getP();
			if (Math.abs(p.getZ()-lm.getLocation().getZ()) < 0.1) {

				p = p.plus(v);
				gl.glPolygonMode(GL.GL_FRONT_AND_BACK, GL.GL_POINT);
				gl.glPointSize(pointSize*2);
				gl.glPushMatrix();
				gl.glBegin(GL.GL_POINTS);

				gl.glColor3f(lm.getDisplayColor().getRed()/256.0f, lm.getDisplayColor().getGreen()/256.0f, lm.getDisplayColor().getBlue()/256.0f);

				gl.glVertex3f(lm.getLocation().getX()*scale, lm.getLocation().getY()*scale, p.getZ()*scale-0.1f);

				gl.glEnd();
				gl.glPointSize(pointSize);
				gl.glPopMatrix();
			}
		}
	}
	
	private void updateTriangles(GL gl) {

		gl.glEnable(GL.GL_POLYGON_OFFSET_FILL);
		gl.glPolygonOffset(1.0f, 1.0f);
		gl.glPolygonMode(GL.GL_FRONT_AND_BACK, GL.GL_FILL);
		colorCallList = app.getCutter().paintCrossSection(gl, FILL, colorCallList);

		gl.glDisable(GL.GL_POLYGON_OFFSET_FILL);
		gl.glPolygonMode(GL.GL_FRONT_AND_BACK, GL.GL_LINE);
		gl.glEnable(GL.GL_LINE_SMOOTH);
		blackCallList = app.getCutter().paintCrossSection(gl,OUTLINE, blackCallList);
		gl.glDisable(GL.GL_LINE_SMOOTH);

		app.getCutter().refresh(false);
	}

	private void mouseOver(GL gl, int x, int y, boolean isShiftClick) {
		if (x == lastMouseOverX && y == lastMouseOverY && !isShiftClick) return; 		//If mouse hasn't moved, no point in bothering GPU...

		lastMouseOverX = x;
		lastMouseOverY = y;

		GLU glu = new GLU();
		int bufferSize = 4*app.getMesh().getTopology().getPolyhedra().size();
		IntBuffer buffer = BufferUtil.newIntBuffer(bufferSize);
		gl.glSelectBuffer(bufferSize, buffer);
		gl.glRenderMode(GL.GL_SELECT);
		gl.glInitNames();
		gl.glPushName(-1);

		gl.glMatrixMode(GL.GL_PROJECTION);
		gl.glLoadIdentity();
		int viewport[] = new int[4];
		gl.glGetIntegerv(GL.GL_VIEWPORT, viewport, 0);

		glu.gluPickMatrix((double) x, (double) (viewport[3] - y), 1.0, 1.0, viewport, 0);
		gl.glOrtho(
				app.getCutter().getXMin()*zoomFactor*aspectRatio, 
				app.getCutter().getXMax()*zoomFactor*aspectRatio, 
				app.getCutter().getYMin()*zoomFactor, 
				app.getCutter().getYMax()*zoomFactor, -250.0f, 250.0f
		);
		gl.glMatrixMode(GL.GL_MODELVIEW);
		gl.glLoadIdentity();
		pan(gl);

		app.getCutter().rotateView(gl);
		app.getCutter().paintCrossSection(gl, FILL, colorCallList);

		int hits = gl.glRenderMode(GL.GL_RENDER);
		reshape(gl, (int)(viewport[2]), (int)( viewport[3]));
		int [] selected = new int[hits*4];
		buffer.get(selected);

		int target = -2; int index = 0, depth = 0;
		for(int i = 0; i < hits; i++) {
			if(index == 0 || selected[index+2] < depth) {
				//Update: Current record is closer
				index++;									//Skip depth of the name stack (always 1, here...)
				depth = selected[index++];    	//Min depth
				index++;                               		//Skip max depth
				target = selected[index++];
			} else {
				//Skip: Record is farther away
				int names = selected[index++];
				index += 2 + names;
			}
		}

		if (target != -2) {
			Polyhedron p = app.getMesh().getTopology().getPolyhedra().get(target);
			Log.setStatus(Mesh.getName(p.getMaterial()));
			if (isShiftClick) {
				if (flipSwitch) {
					app.setActiveMaterials(0,p.getMaterial());
				} else {
					app.setActiveMaterials(1,p.getMaterial());
				}
				flipSwitch = !flipSwitch;
			}
		} else {
			Log.setStatus(" ");
			if (isShiftClick) {
				if (flipSwitch) {
					app.setActiveMaterials(0,-1);
				} else {
					app.setActiveMaterials(1,-1);
				}
				flipSwitch = !flipSwitch;
			}
		}

	}

	//******************************************************************************************************************************
	//
	//			MOUSEADAPTER IMPLEMENTATION
	//
	//******************************************************************************************************************************

	public void mouseDrag(GL gl, int mouseX, int mouseY) {
		double distance = Math.sqrt((mouseX-lastLocX)*(mouseX-lastLocX) + (mouseY-lastLocY)*(mouseY-lastLocY));
		if (distance > 5){
			Vertex location = get3DPoint(gl,mouseX,mouseY);
			Landmark lm = Landmark.getLandmark(app.getActiveMaterials().get(0), app.getActiveMaterials().get(1), location);
			
			app.addLandmark(lm);				
			lastLocX = mouseX;
			lastLocY = mouseY;
		}
	}

	/** Receives a mouse event, and figures out whether it's a click, drag, release, etc., and then calls the appropriate response method */
	public void mouseClick(GL gl, int mouseX, int mouseY) {									// Case: Beginning a new drag
		Vertex location = get3DPoint(gl,mouseX,mouseY);
		Landmark lm = Landmark.getLandmark(app.getActiveMaterials().get(0), app.getActiveMaterials().get(1), location);

		app.addLandmark(lm);			
		lastLocX = mouseX;
		lastLocY = mouseY;
	}

	/** Given the coordinates of the cursor on the the 2D viewport, unprojects the coordinates into 3D space and returns a Vertex object. */
	private Vertex get3DPoint(GL gl, int mouseX, int mouseY) {
		GLU glu = new GLU();

		/* Computing X & Z coordinates */
		double wcoordNear[] = new double[4];// wx, wy, wz;// returned xyz coords
		double wcoordFar[] = new double[4];
		int viewport[] = new int[4]; 					gl.glGetIntegerv(GL.GL_VIEWPORT, viewport, 0);
		double mvmatrix[] = new double[16];		gl.glGetDoublev(GL.GL_MODELVIEW_MATRIX, mvmatrix, 0);
		double projmatrix[] = new double[16];	gl.glGetDoublev(GL.GL_PROJECTION_MATRIX, projmatrix, 0);
		int realy = viewport[3] - (int) mouseY;// ... - 1;  /* note viewport[3] is height of window in pixels */
		glu.gluUnProject((double) mouseX, (double) realy, 0.0,mvmatrix, 0,projmatrix, 0, viewport, 0, wcoordNear, 0);
		glu.gluUnProject((double) mouseX, (double) realy, 1.0,mvmatrix, 0,projmatrix, 0, viewport, 0, wcoordFar, 0);

		/* Computing Z coordinate */
		Vertex linePt1 = new Vertex((float)wcoordNear[0],(float)wcoordNear[1],(float)wcoordNear[2]);
		Vertex linePt2 = new Vertex((float)wcoordFar[0],(float)wcoordFar[1],(float)wcoordFar[2]);
		Vertex planeP = app.getCutter().getP();
		Vector planeN = app.getCutter().getN();
		Vector u = new Vector(linePt1,linePt2);
		Vector w = new Vector(planeP,linePt1);
		float D = planeN.dotProduct(u);
		float N = -planeN.dotProduct(w);
		float sI = N / D;
		Vertex point = linePt1.plus(u.getScaled(sI));
		return point;
	}
	
	protected int panX = 0, panY = 0;
	protected boolean zoomTool = false, panTool = false, pickTool = true;
	public void setZoomTool() { 	zoomTool = true; 	panTool = false; 	pickTool = false; 	glCanvas.setCursor(Cursor.getPredefinedCursor(Cursor.N_RESIZE_CURSOR)); }
	public void setPanTool() { 		zoomTool = false;	panTool = true; 	pickTool = false; 	glCanvas.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR)); 	}
	public void setPickTool() { 		zoomTool = false;	panTool = false;	pickTool = true;	  	glCanvas.setCursor(Cursor.getDefaultCursor()); }

	/** Responds when mouse is clicked, calling selection on the given point */ 	
	public void mouseClicked(MouseEvent e) {
		mouseX = e.getX();
		mouseY = e.getY();
		mouseOverX = e.getX();
		mouseOverY = e.getY();
		mouseReleased = true;
		isDragging = false;
	}
	/** Responds when mouse is dragged, calling selection on the given point*/	
	public void mouseDragged(MouseEvent e) {
		mouseX = e.getX();
		mouseY = e.getY();
		mouseOverX = e.getX();
		mouseOverY = e.getY();
		mouseReleased = false;
		isDragging = true;
	}
	/** Responds when mousewheel scrolls, adjusting the zoom factor */			  	
	public void mouseWheelMoved(MouseWheelEvent e) {
		if (e.getScrollType() == MouseWheelEvent.WHEEL_UNIT_SCROLL) {
			zoomFactor += 0.01f*e.getUnitsToScroll();   // Zoom in or out.
			if (zoomFactor <= 0.001f) zoomFactor = 0.001f;
		}
		zoomChanged = true;
	}
	/** Responds when mouse is released, turning "drag" mode off. */ 					
	public void mouseReleased(MouseEvent e) {	
		isDragging = false;
		mouseReleased = true;
	}
	/** Responds when mouse is moved, updating trackers. */								
	public void mouseMoved(MouseEvent e) 	{
		mouseOverX = e.getX();
		mouseOverY = e.getY();
	}

	public void mouseEntered(MouseEvent e) 	{}
	public void mouseExited(MouseEvent e)	 	{}
	public void mousePressed(MouseEvent e)	{
		isShiftClick = false;
		if (e.isShiftDown()) {
			isShiftClick = true;
		}
		mouseX = e.getX();
		mouseY = e.getY();
		isDragging = false;
		mouseReleased = true;
	}
}
