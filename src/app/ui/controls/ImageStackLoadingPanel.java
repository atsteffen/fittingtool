package app.ui.controls;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

import app.RegistrationTool;
import app.tools.structure.Landmark;
import app.tools.structure.Mesh;
import app.tools.utils.MeshLoaderUtils;
import app.tools.utils.RegistrationUtils;
import app.ui.controls.InteractiveRegistrationPanel.RegistrationMode;

/**
 * Panel for loading a stack of images and extracting landmarks from their boundaries
 * 
 * @author Adam Steffen
 */
public class ImageStackLoadingPanel extends JPanel {
	private static final long serialVersionUID = 1L;

	//-------------------------------------------------------------------
	// Member variables
	//-------------------------------------------------------------------
	
	private RegistrationTool app;	
	private JSpinner fitWeight;
	private JSpinner deformWeight;
	private JSpinner iters;
	private JSpinner thresholdWeight;
	private JSpinner smoothingIters;
	
	//-------------------------------------------------------------------
	// Constructors
	//-------------------------------------------------------------------
	
	public ImageStackLoadingPanel(RegistrationTool app) {
		super();
		this.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		this.app = app;
		init();
	}
	
	//-------------------------------------------------------------------
	// Methods
	//-------------------------------------------------------------------
	
	private void init() {
		
		// loading
		JButton loadNewImageStack = new JButton("Load New Image Stack");
		loadNewImageStack.addActionListener(
				new ActionListener() { 
					@Override	
					public void actionPerformed(ActionEvent e) {
						JFileChooser chooser = new JFileChooser();
						int option = chooser.showOpenDialog(app);
						if (option == JFileChooser.APPROVE_OPTION) {
							File sf = chooser.getSelectedFile();
							try {
								loadImageStreamer(sf);
							} catch (IOException e1) {
								e1.printStackTrace();
							}
						}
					} 
				});
		
		SpinnerNumberModel thresholdModel = new SpinnerNumberModel(0.5, 0, 1.0, 0.01);
		SpinnerNumberModel smoothingModel = new SpinnerNumberModel(1, 1, 1000, 1);
		
		thresholdWeight = new JSpinner(thresholdModel);
		smoothingIters = new JSpinner(smoothingModel);
		
		// extract boundary	
		JButton extractLandmarks = new JButton("Extract Boundary");
		extractLandmarks.addActionListener(
				new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						extractLandmarksFromStack((Double) thresholdWeight.getValue(), (Integer) smoothingIters.getValue());
					}
				});
		
		// boundary extract panel
		JPanel extractPanel1 = new JPanel();
		extractPanel1.setLayout(new BoxLayout(extractPanel1, BoxLayout.Y_AXIS));
		JPanel extractPanel2 = new JPanel();
		extractPanel2.setLayout(new BoxLayout(extractPanel2, BoxLayout.Y_AXIS));
		
		JPanel smoothAndIterations = new JPanel();
		smoothAndIterations.setLayout(new BoxLayout(smoothAndIterations, BoxLayout.X_AXIS));
		
		smoothAndIterations.add(extractLandmarks);
		smoothAndIterations.add(Box.createHorizontalStrut(13));
		smoothAndIterations.add(smoothingIters);
		
		extractPanel1.add(new JLabel("Threshold: "));
		extractPanel1.add(thresholdWeight);
		thresholdWeight.setAlignmentX(Component.LEFT_ALIGNMENT);
		extractPanel2.add(new JLabel("Smoothing iterations: "));
		extractPanel2.add(smoothAndIterations);
		smoothAndIterations.setAlignmentX(Component.LEFT_ALIGNMENT);
		
		// fitting	
		JPanel fitpanel = new JPanel();
		fitpanel.setLayout(new BoxLayout(fitpanel, BoxLayout.Y_AXIS));
		
		SpinnerNumberModel fitModel = new SpinnerNumberModel(1.0, 0, 100, 0.01);
		SpinnerNumberModel deformModel = new SpinnerNumberModel(1.0, 0, 100, 0.01);
		SpinnerNumberModel iterModel = new SpinnerNumberModel(1, 1, 50, 1);
		
		fitWeight = new JSpinner(fitModel);
		deformWeight = new JSpinner(deformModel);
		iters = new JSpinner(iterModel);
		
		JButton fitToBoundary = new JButton("Fit Boundary");
		fitToBoundary.addActionListener(
				new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						Mesh result = RegistrationUtils.addLandmarksToFit(app.getLandmarks(), (Double) fitWeight.getValue(), (Double) deformWeight.getValue(), (Integer) iters.getValue());
						if (app.getRegistrationMode().equals(RegistrationMode.GLOBAL)) {
							app.setBaseMesh(result);
						} else {
							app.setPartialMesh(result);
						}
					}
				});
		
		JPanel fitAndIterations = new JPanel();
		fitAndIterations.setLayout(new BoxLayout(fitAndIterations, BoxLayout.X_AXIS));
		
		fitAndIterations.add(fitToBoundary);
		fitToBoundary.setAlignmentX(Component.LEFT_ALIGNMENT);
		fitAndIterations.add(Box.createHorizontalStrut(13));
		fitAndIterations.add(iters);
		
		// saving		
		JPanel buttonpanel = new JPanel();
		buttonpanel.setLayout(new BoxLayout(buttonpanel, BoxLayout.Y_AXIS));
		
		JButton saveMeshBoundary = new JButton("Fix Atlas Boundary");
		saveMeshBoundary.addActionListener(
				new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						Mesh result = RegistrationUtils.saveRegistrationSession();
						if (app.getRegistrationMode().equals(RegistrationMode.GLOBAL)) {
							app.setBaseMesh(result);
						} else {
							app.setPartialMesh(result);
						}
						app.clearLandmarks();
					}
				});
		
		JPanel fitpanelLabel = new JPanel();
		fitpanelLabel.setLayout(new BoxLayout(fitpanelLabel, BoxLayout.X_AXIS));
		fitpanelLabel.add(new JLabel("(step 3)"));
		fitpanelLabel.add(Box.createHorizontalGlue());
		
		fitpanel.add(Box.createVerticalGlue());
		fitpanel.add(fitpanelLabel);
		fitpanel.add(new JLabel("Fit Weight"));
		fitpanel.add(fitWeight);
		fitpanel.add(Box.createVerticalStrut(13));
		fitpanel.add(new JLabel("Iterations"));
		fitpanel.add(fitAndIterations);
		fitpanel.add(Box.createVerticalStrut(13));
		
		JPanel savePanelLabel = new JPanel();
		savePanelLabel.setLayout(new BoxLayout(savePanelLabel, BoxLayout.X_AXIS));
		savePanelLabel.add(new JLabel("(step 4)"));
		savePanelLabel.add(Box.createHorizontalGlue());

		JPanel savePanelButton = new JPanel();
		savePanelButton.setLayout(new BoxLayout(savePanelButton, BoxLayout.X_AXIS));
		savePanelButton.add(saveMeshBoundary);
		savePanelButton.add(Box.createHorizontalGlue());
		
		fitpanel.add(savePanelLabel);
		fitpanel.add(savePanelButton);
		
		buttonpanel.add(new JLabel("(step 1)"));
		buttonpanel.add(loadNewImageStack);
		buttonpanel.add(Box.createVerticalStrut(13));
		buttonpanel.add(new JLabel("(step 2)"));
		buttonpanel.add(extractPanel1);
		buttonpanel.add(Box.createVerticalStrut(13));
		buttonpanel.add(extractPanel2);
		buttonpanel.add(Box.createVerticalGlue());
		
		add(Box.createHorizontalStrut(20));
		add(buttonpanel);
		add(Box.createHorizontalStrut(20));
		add(fitpanel);
		add(Box.createHorizontalStrut(20));
	}
	
	private void loadImageStreamer(File file) throws IOException {
		ImageStreamer streamer = MeshLoaderUtils.loadImageStreamer(app, file.getParent(),file.getName());
		if (!ImageStreamer.Z_AXIS.equals("null")) {
			streamer.setId(ImageStreamer.Z_AXIS);
			streamer.setAxis(app.getCutter().getN().getNormalized());
		}
		
		app.getViewer2D().setStreamer(streamer);
		app.getCutter().setTicks();

	}
	
	private void extractLandmarksFromStack(double threshold, int smoothingIterations) {
		ImageStreamer streamer = app.getViewer2D().getStreamer();	
		if (streamer == null) return;
		List<Landmark> landmarks = streamer.extractLandmarks(threshold,smoothingIterations);
		app.setLandmarks(landmarks);
		app.getMesh().pcaAlignToLandmarks(landmarks,app.getBaseMesh());
		app.reloadMesh();
	}

}
