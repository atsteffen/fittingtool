package app.ui.controls;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JSeparator;

import app.RegistrationTool;
import app.tools.utils.MeshLoaderUtils;

public class MeshMenu extends JMenuBar {
	private static final long serialVersionUID = 1L;

	//-------------------------------------------------------------------
	// Member variables
	//-------------------------------------------------------------------
	
	private JFileChooser chooser_landmarks;
	private JFileChooser chooser_mesh;
	private JFileChooser chooser_save;
	
	private RegistrationTool app;

	//-------------------------------------------------------------------
	// Constructors
	//-------------------------------------------------------------------
	
	public MeshMenu(RegistrationTool app) {
		super();
		this.app = app;
		init();
	}

	//-------------------------------------------------------------------
	// Methods: initialize
	//-------------------------------------------------------------------

	private void init() {
		JMenu filemenu = new JMenu("File");
		JMenuItem fileItem2 = new JMenuItem("Load Atlas");
		JMenuItem fileItem3 = new JMenuItem("Import Landmarks");
		JMenuItem fileItem4 = new JMenuItem("Save Landmarks");
		JMenuItem fileItem5 = new JMenuItem("Save Atlas");
		chooser_mesh = new JFileChooser();
		chooser_landmarks = new JFileChooser();
		chooser_save = new JFileChooser();
		fileItem2.addActionListener(
				new ActionListener() { 
					@Override	
					public void actionPerformed(ActionEvent e) {
						int option = chooser_mesh.showOpenDialog(app);
						if (option == JFileChooser.APPROVE_OPTION) {
							File sf = chooser_mesh.getSelectedFile();
							app.loadMesh(sf);
						}
					}
				});
		fileItem3.addActionListener(
				new ActionListener() { 
					@Override	
					public void actionPerformed(ActionEvent e) {
						int option = chooser_landmarks.showOpenDialog(app);
						if (option == JFileChooser.APPROVE_OPTION) {
							File sf = chooser_landmarks.getSelectedFile();
							try {
								app.loadLandmarks(sf.getCanonicalPath());
							} catch (IOException e1) {
								e1.printStackTrace();
							}
						}
					}
				});
		fileItem4.addActionListener(
				new ActionListener() { 
					@Override	
					public void actionPerformed(ActionEvent e) {
						int option = chooser_landmarks.showSaveDialog(app);
						if (option == JFileChooser.APPROVE_OPTION) {
							File sf = chooser_landmarks.getSelectedFile();
							try {
								MeshLoaderUtils.writeLandmarks(app.getLandmarks(), sf);
							} catch (IOException e1) {
								e1.printStackTrace();
							}
						}
					}
				});
		fileItem5.addActionListener(
				new ActionListener() { 
					@Override	
					public void actionPerformed(ActionEvent e) {
						int option = chooser_save.showSaveDialog(app);
						if (option == JFileChooser.APPROVE_OPTION) {
							File sf = chooser_save.getSelectedFile();
							try {
								app.writeMeta(sf.getCanonicalPath()+".meta");
								app.writeMesh(sf.getCanonicalPath());
							} catch (IOException e1) {
								e1.printStackTrace();
							}
						}
					}
				});
		filemenu.add(fileItem2);
		filemenu.add(new JSeparator());
		filemenu.add(fileItem3);
		filemenu.add(fileItem4);
		filemenu.add(new JSeparator());
		filemenu.add(fileItem5);
		this.add(filemenu);
	}
}
