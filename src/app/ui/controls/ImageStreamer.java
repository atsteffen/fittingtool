package app.ui.controls;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

import javax.media.opengl.GL;

import com.sun.opengl.util.texture.Texture;
import com.sun.opengl.util.texture.TextureIO;

import app.RegistrationTool;
import app.tools.structure.Landmark;
import app.tools.structure.Vector;
import app.tools.topology.Vertex;
import app.tools.utils.FileUtils;
import app.tools.utils.ImageUtils;

public class ImageStreamer {
	
	public static final String X_AXIS = "x", Y_AXIS = "y", Z_AXIS = "z";
	private static final int LOW_TEX = 128;

	//-------------------------------------------------------------------
	// Properties
	//-------------------------------------------------------------------
	
	private float minX,minY,minZ;
	private float maxX,maxY,maxZ;
	private float spacingx, spacingy, spacingz;
	private Vertex origin;
	private Texture curTexture;
	private int curIndex, currentsize, sizestart;
	private int sizex, sizey, sizez;
	private int scale;
	private String id;
	private String ext, base, directory;
	private Vector axis;
	
	private RegistrationTool app;
	
	//-------------------------------------------------------------------
	// Constructors
	//-------------------------------------------------------------------

	public ImageStreamer(RegistrationTool app, int sizestart, int sizex, int sizey, int sizez, String base,
			String ext, int scale, String directory) {
		this.sizestart = sizestart;
		this.sizex = sizex;
		this.sizey = sizey;
		this.sizez = sizez;
		this.base = base;
		this.ext = ext;
		this.scale = scale;
		this.directory = directory;
		currentsize = 0;
		this.app = app;
		
		minX = app.getCutter().getXMin();
		maxX = app.getCutter().getXMax();
		minY = app.getCutter().getYMin();
		maxY = app.getCutter().getYMax();
		minZ = app.getCutter().getZMin();
		maxZ = app.getCutter().getZMax();
		
		origin = new Vertex((maxX-minX)/2.0f+minX,(maxY-minY)/2.0f+minY,(maxZ-minZ)/2.0f+minZ);
		
		int midx = (int) java.lang.Math.floor(sizex/2.0f);
		spacingx = (java.lang.Math.abs(maxX-minX)/2.0f)/midx;
		int midy = (int) java.lang.Math.floor(sizey/2.0f);
		spacingy = (java.lang.Math.abs(maxY-minY)/2.0f)/midy;
		int midz = (int) java.lang.Math.floor(sizez/2.0f);
		spacingz = (java.lang.Math.abs(maxZ-minZ)/2.0f)/midz;
	}
	
	//-------------------------------------------------------------------
	// Methods
	//-------------------------------------------------------------------
	
	public List<Landmark> extractLandmarks(double threshold, int smoothIters) {
		List<Landmark> landmarks = new ArrayList<Landmark>();
		
		// For now just use the z-aligned images for aligning the boundary
		for (int i=sizestart + sizex + sizey; i<sizex+sizey+sizez+sizestart; i++) {
			landmarks.addAll(getBoundaryPoints(getImage(i, true),i-sizestart-sizey-sizex,threshold,smoothIters)); 			
		}
		return landmarks;
	}

	public void paintImage(GL gl, Vertex center) {

		gl.glPushMatrix();

		gl.glEnable(GL.GL_TEXTURE_2D);
		gl.glTexEnvf(GL.GL_TEXTURE_ENV, GL.GL_TEXTURE_ENV_MODE, GL.GL_MODULATE);

		gl.glPolygonMode(GL.GL_FRONT_AND_BACK, GL.GL_FILL);
		final float safeDistance =-2f*app.getCutter().getRadius();

		gl.glColor3f(1.0f, 1.0f,1.0f);

		int index = getIndex(center);
		try { getTexture(index).bind(); } 	//Images aren't available for all cutting-plane locations.
		catch (NullPointerException e ) { gl.glDisable(GL.GL_TEXTURE_2D); } 
		curIndex = index;	

		app.getCutter().rotateView(gl);

		center.setXYZ(new float[] {center.getX()-0.04541f, center.getY() - 0.30283353f,    center.getZ() });

		float minXcoord = 0.0f;
		float maxXcoord = 0.0f;
		float minYcoord = 0.0f;
		float maxYcoord = 0.0f;
		if (getId().equals("z")){
			minXcoord = minX;
			maxXcoord = maxX;
			minYcoord = minY;
			maxYcoord = maxY;
		} else if (getId().equals("y")){
			minXcoord = minX;
			maxXcoord = maxX;
			minYcoord = minZ;
			maxYcoord = maxZ;
		} else if (getId().equals("x")){
			minXcoord = minZ;
			maxXcoord = maxZ;
			minYcoord = minY;
			maxYcoord = maxY;
		}

		gl.glBegin(GL.GL_QUADS);
		gl.glTexCoord2d(1.0, 1.0);
		gl.glVertex3f(	maxXcoord, 		minYcoord, 	 	safeDistance);	
		gl.glTexCoord2d(0.0, 1.0);
		gl.glVertex3f(	minXcoord, 		minYcoord, 	 	safeDistance);
		gl.glTexCoord2d(0.0, 0.0);
		gl.glVertex3f(	minXcoord, 		maxYcoord, 		safeDistance);
		gl.glTexCoord2d(1.0, 0.0);
		gl.glVertex3f(	maxXcoord, 		maxYcoord, 		safeDistance);
		gl.glEnd();

		gl.glDisable(GL.GL_TEXTURE_2D);
		gl.glPopMatrix();
	}
	
	//-------------------------------------------------------------------
	// Getters and Setters
	//-------------------------------------------------------------------
	
	public float getSpacingz() { return spacingz; }
	
	public float getMinZ() { return minZ; }
	
	public float getMaxZ() { return maxZ; }
	
	public Vertex getOrigin() { return origin; }
	
	public int getSizeZ() { return sizez; }
	
	public String getId() { return id; }

	public void setId(String id) {
		this.id = id;
		if (getId() == "x") {
			currentsize = sizex;
		}
		if (getId() == "y") {
			currentsize = sizey;
		}
		if (getId() == "z") {
			currentsize = sizez;
		}
	}
	
	public Vector getAxis() { return axis; }

	public void setAxis(Vector axis) { this.axis = axis; }
	
	//-------------------------------------------------------------------
	// Helper Methods
	//-------------------------------------------------------------------

	/** Loads thumbnail images from GeneServer.
	 * Full-resolution images are streamed in another thread by FTPTask objects.
	 * @param index
	 * @return image
	 */
	private BufferedImage getImage(int index, boolean lowres) {		
		BufferedImage image = FileUtils.getImage(index, directory, base, ext, scale);
		int height = image.getHeight();
		int width = image.getWidth();
		if (lowres) {
			height = LOW_TEX;
			width = LOW_TEX;
		}
		BufferedImage scaledImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);

		// Paint scaled version of image to new image
		Graphics2D graphics2D = scaledImage.createGraphics();
		graphics2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		graphics2D.drawImage(image, 0, 0, width, height, null);
		graphics2D.dispose();

		return scaledImage;
	}

	/** Given a BufferedImage, generates a new Texture object ready for binding.
	 * Requires that there be an active GL context on the current thread.
	 * 
	 * @param image
	 * @return texture
	 */
	private Texture getTexture(BufferedImage image) {
		Texture texture =  TextureIO.newTexture(image, false);
		texture.setTexParameteri(GL.GL_TEXTURE_WRAP_S, GL.GL_REPEAT);
		texture.setTexParameteri(GL.GL_TEXTURE_WRAP_T, GL.GL_REPEAT);
		texture.setTexParameteri(GL.GL_TEXTURE_MAG_FILTER, GL.GL_LINEAR);
		texture.setTexParameteri(GL.GL_TEXTURE_MIN_FILTER, GL.GL_LINEAR);
		texture.bind();
		return texture;
	}

	private Texture getTexture(int i) {
		if (i < 0 || i > currentsize) return null;
		if (i == curIndex) 	{			
			return curTexture;
		}
		setTexture(getTexture(getImage(i+sizestart, false)));
		return curTexture;
	}

	/** Sets the current texture to the one provided, clearing out old resources first. */
	private void setTexture(Texture t) {
		if (curTexture != null) curTexture.dispose();
		curTexture = t;
	}

	private int getIndex(Vertex p) {

		if (getId() == "z" ) {													//Z-Stack. Middle image centered.
			return (int) Math.max(Math.min(sizez-1,(p.getZ()-minZ)/spacingz),0);
		} else if (getId() == "y" ) {											//Y-Stack. Top down.
			int offset = (int) (new Vector(p,origin).dotProduct(axis)/spacingy);
			offset += (int) java.lang.Math.floor(sizey/2.0f);
			return offset + sizestart + sizex;
		} else if (getId() == "x") {											//X-Stack. Left to right.
			int offset = (int) (new Vector(origin,p).dotProduct(axis)/spacingx);
			offset += java.lang.Math.floor(sizex/2.0f);
			return offset + sizestart;
		} else {
			throw new RuntimeException("Metadata error: No axis maps to Z. Unable to determine image index.");
		}
	}

	private List<Landmark> getBoundaryPoints(BufferedImage bufferedImage, int index, double threshold, int smoothIters) {
		// Get all the pixels
		int w = bufferedImage.getWidth(null);
		int h = bufferedImage.getHeight(null);
		int[] matrix = new int[w*h];
		
	    for(int i=0; i < w; i ++) {
	        for(int j=0; j < h; j++) {
	            //Grab and set the colors one-by-one
	            int argb = bufferedImage.getRGB(i, j);
                int r = (argb >> 16) & 0xff;
                int g = (argb >>  8) & 0xff;
                int b = (argb      ) & 0xff;

                int l = (int) (.299 * r + .587 * g + .114 * b);
                double ratio = l/256.0;
	            if (ratio >= threshold) {
	            	matrix[h*i+j] = 1;
	            } else {
	            	matrix[h*i+j] = 0;
	            }
	        }
	    }
	    //ImageUtils.openSmooth(matrix,w,h,smoothIters);
	    ImageUtils.largestConnectForeground(matrix,w,h);
	    ImageUtils.largestConnectBackground(matrix,w,h);
	    ImageUtils.openSmooth(matrix,w,h,smoothIters);
	    List<Integer> boundary = ImageUtils.getBoundaryPixels(matrix,w,h);
	    
	    List<Landmark> result = new ArrayList<Landmark>();
	    for (Integer i : boundary) {
	    	int xint = i/h;
			int yint = h-i%h;
			float xcoord = (xint/(float)w) * (maxX-minX) + minX;
			float ycoord = (yint/(float)h) * (maxY-minY) + minY;
			float zcoord = (index/(float)sizez) * (maxZ-minZ) + minZ;
						
			Landmark lm = new Landmark(new Vertex(xcoord,ycoord,zcoord));
			lm.addMaterial(-1);
			lm.addMaterial(-2);
			lm.setDisplayColor(new Color(1.0f, 1.0f, 0.0f));
			
			if (java.lang.Math.random() > 0.9) {
				result.add(lm);
			}
	    }	    
	    return result;
	}
}