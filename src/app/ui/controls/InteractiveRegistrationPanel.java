package app.ui.controls;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;

import app.RegistrationTool;
import app.tools.structure.Mesh;
import app.tools.utils.Log;
import app.tools.utils.RegistrationUtils;

public class InteractiveRegistrationPanel extends JPanel {
	private static final long serialVersionUID = 1L;
	
	public static enum RegistrationMode { LOCAL, GLOBAL };

	//-------------------------------------------------------------------
	// Member variables
	//-------------------------------------------------------------------
	
	private RegistrationTool app;
	private JSpinner fitWeight;
	private JSpinner deformWeight;
	private JSpinner iters;	
	private JComboBox boundary1;
	private JComboBox boundary2;
	
	//-------------------------------------------------------------------
	// Constructors
	//-------------------------------------------------------------------
	
	public InteractiveRegistrationPanel(RegistrationTool app) {
		super();
		this.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		this.app = app;
		init();
	}
	
	//-------------------------------------------------------------------
	// Methods: layout
	//-------------------------------------------------------------------
	
	private void init() {
		JPanel regpanel = new JPanel();
		regpanel.setLayout(new BoxLayout(regpanel, BoxLayout.Y_AXIS));
	
		SpinnerNumberModel fitModel = new SpinnerNumberModel(1.0, 0, 100, 0.01);
		SpinnerNumberModel deformModel = new SpinnerNumberModel(1.0, 0, 100, 0.01);
		SpinnerNumberModel iterModel = new SpinnerNumberModel(1, 1, 50, 1);
		
		fitWeight = new JSpinner(fitModel);
		deformWeight = new JSpinner(deformModel);
		iters = new JSpinner(iterModel);
		
		JButton clearregButton = new JButton("Clear");
		clearregButton.addActionListener(new ClearListener());
		
		JButton fitregButton = new JButton("Fit");
		fitregButton.addActionListener(new FitListener());
		
		JButton saveregButton = new JButton("Save");
		saveregButton.addActionListener(new SaveListener());
		
		regpanel.add(new JLabel("Fit Weight"));
		regpanel.add(fitWeight);
		regpanel.add(new JLabel("Deform Weight"));
		regpanel.add(deformWeight);
		regpanel.add(new JLabel("iterations"));
		regpanel.add(iters);
		
		JPanel regsubpanel = new JPanel();
		regsubpanel.setLayout(new BoxLayout(regsubpanel, BoxLayout.X_AXIS));
		regsubpanel.add(clearregButton);
		regsubpanel.add(fitregButton);
		regsubpanel.add(saveregButton);
		
		regpanel.add(regsubpanel);
		
		// Boundary Picker interface
		JPanel boundaryPicker = new JPanel();
		boundaryPicker.setLayout(new BoxLayout(boundaryPicker, BoxLayout.Y_AXIS));

		ArrayList<Integer> ids2 = new ArrayList<Integer>();
		if (app.getMesh() != null) {
			ids2 = app.getMesh().getPartitionIDs();
		}
		String[] boundaries = new String[ids2.size()+2];
		boundaries[0] = "Unknown Region";
		boundaries[1] = "Empty Region";
		for (int i = 2; i < ids2.size()+2; ++i) {
			boundaries[i] = Mesh.getName(ids2.get(i-2));
		}
		boundary1 = new JComboBox(boundaries);
		boundary1.setSelectedIndex(0);
		app.setActiveMaterials(0,-2);
		boundary1.addActionListener(new BoundaryOneListener());
		
		boundary1.setMaximumRowCount(5);

		boundary2 = new JComboBox(boundaries);
		boundary2.setSelectedIndex(1);
		app.setActiveMaterials(1,-1);
		
		boundary2.addActionListener(new BoundaryTwoListener());
		
		boundary2.setMaximumRowCount(4);

		boundaryPicker.add(new JLabel("Boundary 1:"));
		boundaryPicker.add(boundary1);
		boundaryPicker.add(new JLabel("Boundary 2:"));
		boundaryPicker.add(boundary2);
		
		JRadioButton local = new JRadioButton("Local"), global = new JRadioButton("Global");
		local.setActionCommand("" + 0);
		global.setActionCommand("" + 1);
		local.setToolTipText("Fit to landmarks of the current boundary only");
		global.setToolTipText("Fit to landmarks accross all regions");
		ScopeListener axLis = new ScopeListener();
		local.addActionListener(axLis);
		global.addActionListener(axLis);
		global.setSelected(true);
		ButtonGroup aB = new ButtonGroup();
		aB.add(local);
		aB.add(global);
		
		boundaryPicker.add(new JLabel("Registration mode:"));
		boundaryPicker.add(local);
		boundaryPicker.add(global);
		boundaryPicker.add(Box.createVerticalStrut(30));
	

		add(Box.createHorizontalGlue());
		add(regpanel);
		add(Box.createHorizontalStrut(20));
		add(boundaryPicker);
		add(Box.createHorizontalGlue());
	}
	
	//-------------------------------------------------------------------
	// Inner classes: Action listeners
	//-------------------------------------------------------------------
	
	class ScopeListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			switch (Integer.parseInt(arg0.getActionCommand())) {
			case 0:
				app.setRegistrationMode(RegistrationMode.LOCAL);
				app.clearLandmarks();
				break;
			case 1:
				app.setRegistrationMode(RegistrationMode.GLOBAL);
				break;
			default:
				Log.log("ERROR: Registration mode out of bounds.");
			}
		}

	}
	
	class BoundaryTwoListener implements ActionListener {
		@Override	
		public void actionPerformed(ActionEvent e) {
			ArrayList<Integer> ids2 = app.getMesh().getPartitionIDs();
			JComboBox cb = (JComboBox)e.getSource();
			int index = cb.getSelectedIndex();
			if (index > 1) {
				index = ids2.get(index-2);
			}
			else {
				index = index - 2;
			}
			if (app.getRegistrationMode().equals(RegistrationMode.LOCAL) && app.getActiveMaterials().get(1) != index) {
				app.clearLandmarks();
			}
			app.setActiveMaterials(1,index);
		}
	}
	
	class BoundaryOneListener implements ActionListener {
		@Override	
		public void actionPerformed(ActionEvent e) {
			ArrayList<Integer> ids2 = app.getMesh().getPartitionIDs();
			JComboBox cb = (JComboBox)e.getSource();
			int index = cb.getSelectedIndex();
			if (index > 1) {
				index = ids2.get(index-2);
			}
			else {
				index = index - 2;
			}
			if (app.getRegistrationMode().equals(RegistrationMode.LOCAL) && app.getActiveMaterials().get(0) != index) {
				app.clearLandmarks();
			}
			app.setActiveMaterials(0,index);
		}
	}
	
	class SaveListener implements ActionListener {
		@Override	
		public void actionPerformed(ActionEvent e) {
			Mesh result = RegistrationUtils.saveRegistrationSession();
			if (app.getRegistrationMode().equals(RegistrationMode.GLOBAL)) {
				app.setBaseMesh(result);
			} else {
				try {
					RegistrationUtils.saveRegistrationSession(app.getBaseMesh());
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
			app.clearLandmarks();
		}
	}
	
	class ClearListener implements ActionListener {
		@Override	
		public void actionPerformed(ActionEvent e) {
			Mesh result = RegistrationUtils.resetRegistrationSession();
			if (result != null) {
				app.setBaseMesh(result);
			}
			app.clearLandmarks();
		} 
	}
	
	class FitListener implements ActionListener {
		@Override	
		public void actionPerformed(ActionEvent e) {
			Double fit =1.0;
			Double def =1.0;
			Integer it =1;
			SpinnerModel dataModel = fitWeight.getModel();
			if (dataModel instanceof SpinnerNumberModel) {
				fit = (Double) (  (SpinnerNumberModel)dataModel ).getValue();
			}
			dataModel = deformWeight.getModel();
			if (dataModel instanceof SpinnerNumberModel) {
				def = (Double) (  (SpinnerNumberModel)dataModel ).getValue();
			}
			dataModel = iters.getModel();
			if (dataModel instanceof SpinnerNumberModel) {
				it = (Integer) (  (SpinnerNumberModel)dataModel ).getValue();
			}
			if (app.getRegistrationMode().equals(RegistrationMode.GLOBAL)) {
				Mesh result = RegistrationUtils.addLandmarksToFit(app.getLandmarks(), fit, def, it);
				app.setBaseMesh(result);
			} else {
				try {
					Mesh result = RegistrationUtils.localRegistration(app.getLandmarks(), fit, def, it, app.getPartialMesh(), app.getActiveMaterials());
					if (result == null) return;
					app.setPartialMesh(result);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		}
	}

}
