package app.ui.controls;

import java.awt.GridLayout;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import app.RegistrationTool;
import app.ui.gl.Viewer3D;

public class TabbedOptions extends JPanel {	
	private static final long serialVersionUID = 1L;

	//-------------------------------------------------------------------
	// Member variables
	//-------------------------------------------------------------------
	
	private RegistrationTool app;
	private JTabbedPane tabs1;
	private JTabbedPane tabs2;
	
	private VisualizationPanel visPanel;
	private ImageStackLoadingPanel stackPanel;
	private InteractiveRegistrationPanel regPanel;

	//-------------------------------------------------------------------
	// Constructors
	//-------------------------------------------------------------------
	
	public TabbedOptions(RegistrationTool app, Viewer3D view) {
		super();
		this.app = app;
		this.setLayout(new GridLayout());
		init();
	}
	
	//-------------------------------------------------------------------
	// Methods: initialize
	//-------------------------------------------------------------------

	public void init() {	
		super.removeAll();
		
		visPanel = new VisualizationPanel(app);
		stackPanel = new ImageStackLoadingPanel(app);
		regPanel = new InteractiveRegistrationPanel(app);
		
		tabs1 = new JTabbedPane();
		tabs1.add("Atlas Visualization", visPanel);
		tabs2 = new JTabbedPane();
		tabs2.add("Load Image Stack", stackPanel);
		tabs2.add("Register Atlas to Landmarks", regPanel);

		super.add(tabs1);
		super.add(tabs2);
	}
}
