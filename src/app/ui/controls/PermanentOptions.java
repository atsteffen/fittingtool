package app.ui.controls;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedInputStream;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JToggleButton;

import app.RegistrationTool;
import app.data.Bin;
import app.tools.utils.Log;
import app.ui.gl.Viewer2D;
import app.ui.gl.Viewer3D;

public class PermanentOptions extends JPanel {
	private static final long serialVersionUID = 1L;
	
	//-------------------------------------------------------------------
	// Member variables
	//-------------------------------------------------------------------
	
	private RegistrationTool app;
	
	private Integer imageSize = 20;
	private Color backgroundColor = Color.LIGHT_GRAY;
	private Image backgroundImage = null;

	//-------------------------------------------------------------------
	// Constructors
	//-------------------------------------------------------------------
	
	public PermanentOptions (RegistrationTool mom) {
		this.app = mom;
		this.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		init();
	}

	//-------------------------------------------------------------------
	// Methods
	//-------------------------------------------------------------------
	
	private void init() {		
		add(Box.createGlue());
		JToggleButton points = new JToggleButton(getJarIcon("points.png",imageSize),true), edges = new JToggleButton(getJarIcon("edges.png",imageSize),true), faces = new JToggleButton(getJarIcon("faces.png",imageSize));
		VisibilityListener viewLis = new VisibilityListener();
		points.setActionCommand(""+Viewer3D.ELEMENTS_POINTS);
		points.setToolTipText("Show/hide atlas points on 3D viewer");
		edges.setActionCommand(""+Viewer3D.ELEMENTS_EDGES);
		edges.setToolTipText("Show/hide atlas edges on 3D viewer");
		faces.setActionCommand(""+Viewer3D.ELEMENTS_FACES);
		faces.setToolTipText("Show/hide atlas faces on 3D viewer");
		points.addActionListener(viewLis);
		edges.addActionListener(viewLis);
		faces.addActionListener(viewLis);
		add(points);
		add(edges);
		add(faces);

		add(Box.createGlue());
		add(Log.getStatusPanel());
		add(Box.createGlue());
		
		JToggleButton points_2d = new JToggleButton(getJarIcon("points.png",imageSize),true);
		JToggleButton edges_2d = new JToggleButton(getJarIcon("edge_2d.gif",imageSize),true);
		JToggleButton color_2d = new JToggleButton(getJarIcon("color.gif",imageSize));
		JToggleButton image_2d = new JToggleButton(getJarIcon("slice.gif",imageSize), true);
		VisibilityListener2D viewLis2 = new VisibilityListener2D();
		points_2d.setActionCommand(""+Viewer2D.ELEMENTS_POINTS);
		points_2d.setToolTipText("Show/hide atlas points on 2D viewer");
		edges_2d.setActionCommand(""+Viewer2D.ELEMENTS_EDGES);
		edges_2d.setToolTipText("Show/hide atlas edges on 2D viewer");
		color_2d.setActionCommand(""+Viewer2D.ELEMENTS_FACES);
		color_2d.setToolTipText("Show/hide atlas region color on 2D viewer");
		image_2d.setActionCommand(""+Viewer2D.ELEMENTS_IMAGE);
		image_2d.setToolTipText("Show/hide image stack on 2D viewer");
		points_2d.addActionListener(viewLis2);
		edges_2d.addActionListener(viewLis2);
		color_2d.addActionListener(viewLis2);
		image_2d.addActionListener(viewLis2);
		add(points_2d);
		add(edges_2d);
		add(color_2d);
		add(image_2d);
		add(Box.createGlue());

		setBackground();
	}
	
	private static ImageIcon getJarIcon(String name, int size) {
		String path = "icons/" + name;
		int MAX_IMAGE_SIZE = 5000;  //Change this to the size of your biggest image, in bytes.
		int count = 0;
		BufferedInputStream imgStream = new BufferedInputStream(
				Bin.getResourceClass().getResourceAsStream(path));
		byte buf[] = new byte[MAX_IMAGE_SIZE];
		try {
			count = imgStream.read(buf);
			imgStream.close();
		} catch (java.io.IOException ioe) {
			System.err.println("Couldn't read stream from file: " + path);
			return null;
		}
		if (count <= 0) {
			System.err.println("Empty file: " + path);
			return null;
		}
		Image img = Toolkit.getDefaultToolkit().createImage(buf);
		img = img.getScaledInstance(size,size, Image.SCALE_SMOOTH);
		return new ImageIcon(img);	 
	}

	private void setBackground() {
		try { 
			URL imageURL = Bin.getResourceClass().getResource("wood_panel.jpg");
			backgroundImage = ImageIO.read(imageURL);	
			this.setOpaque(false);
		}
		catch (Exception e) { Log.log("Background image not loaded: " + e); this.setBackground(backgroundColor); }
	}

	@Override
	/** Overrides the default paint method, allowing the background image to be painted first. 
	 * @param g
	 */
	public void paint(Graphics g) {
		if (backgroundImage != null) 
			g.drawImage(backgroundImage, 0,0, this.getWidth(), this.getHeight(), this);
		super.paint(g);
	}
	
	//-------------------------------------------------------------------
	// Inner classes
	//-------------------------------------------------------------------

	/** Listener that switches toggles visibility of points, edges and faces **/
	class VisibilityListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			app.getViewer3D().setVisible(Short.parseShort(e.getActionCommand()), ((JToggleButton)e.getSource()).isSelected());
		}

	}
	
	/** Listener that switches toggles visibility of points, edges and faces **/
	class VisibilityListener2D implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			app.getViewer2D().setVisible(Short.parseShort(e.getActionCommand()), ((JToggleButton)e.getSource()).isSelected());
		}

	}
}
