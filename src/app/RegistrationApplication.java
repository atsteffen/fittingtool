package app;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.UIManager;

import app.tools.utils.Log;
import app.ui.controls.MeshMenu;

/**
 * A java application for fitting a subdivision mesh to a set of target landmarks.  The openGL panels, the mesh infrastructure
 * and much of the UI design is based on the SelectionTool applet designed by Don McCurdy.
 * 
 * @author Adam Steffen
 */
public class RegistrationApplication {

	/**
	 * Main Application - launch the frame and initialize the model ({@link RegistrationTool})
	 * @param args
	 */
	public static void main(String[] args) {
		
		Log.div("Launching: GeneAtlas Registration Tool - Application");
		
		// set look-and-feel
		try {
			System.setProperty("apple.laf.useScreenMenuBar", "true");
			System.setProperty("com.apple.mrj.application.apple.menu.about.name", "Fitting Tool");
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) { 
			Log.warn("Look-and-feel could not be altered."); 
		}

		// initialize frame and model
		JFrame frame = new JFrame();
		frame.setSize(1050,700);
		final RegistrationTool prog = new RegistrationTool(frame);
		frame.add(prog, BorderLayout.CENTER);

		// start rendering openGL views
		prog.start();
	
		// add io-menu
		frame.setJMenuBar(new MeshMenu(prog));
		frame.setVisible(true);
	}

}
